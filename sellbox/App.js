import React, { useEffect, createContext, useState } from 'react';
import Routes from '../sellbox/src/routes/Routes'
import { createStore } from 'redux';
import reducer from '../sellbox/src/stores/reducer';
import { Provider } from 'react-redux/'
const store = createStore(reducer);
const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <Routes></Routes>
    </Provider>
  );
};
export default App;