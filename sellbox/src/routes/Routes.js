
import { Dashboard, Explore, Inbox, Home, Settings, Dashboards, SearchTab, SellBoxDetails, TrendingNews } from '../screens/component/Index'
import LogIn from '../../src/screens/login/LogIn'
import * as React from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { setLogINUser, setCheckUserLogin } from "../stores/actions";
import { CommonFunctions, ServerConstant, Loader } from '../screens/commoncomponent/index';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import reducer from '../stores/reducer';
import Demo from '../screens/commoncomponent/Demo'
// import ConfigFirebase from '../screens/commoncomponent/ConfigFirebase'
// import firebase from '@react-native-firebase/app';
// import iid from '@react-native-firebase/iid';
// import messaging from '@react-native-firebase/messaging'
import { Fonts, Colors, Images } from '../styles/Index'
import { View, Text, Image, } from 'react-native';
import SellBoxTrendDetails from '../screens/commoncomponent/SellBoxTrendDetails'
import DummyChart from '../screens/component/dummyChart';

import LogInScreen from "../screens/login/LogInScreen";

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
enableScreens();
const LogInSellBox = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }} initialRouteName={'LogIn'}  >
            <Stack.Screen name="LogIn" component={LogIn} />
            {/* <Stack.Screen name="DummyChart" component={DummyChart}></Stack.Screen> */}
            <Stack.Screen name='LogInScreen' component={LogInScreen}></Stack.Screen>
        </Stack.Navigator>

    )
}
const StackNavigationSellBoxMain = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }} initialRouteName={"Home"} >
            <Stack.Screen name="Home" component={TabNavigatorDeepsense} />
            <Stack.Screen name="Settings" component={Settings} />
            <Stack.Screen name="SellBoxTrendDetails" component={SellBoxTrendDetails} />
        </Stack.Navigator>

    )
}
const SellBoxSearchNavigation = () => {
    return (
        <Stack.Navigator screenOptions={{
            gestureEnabled: true,
            headerShown: false
        }} initialRouteName={"SearchTab"}   >
            <Stack.Screen name="SearchTab" component={SearchTab} />
            <Stack.Screen name="SellBoxDetails" component={SellBoxDetails} />
            <Stack.Screen name="SellBoxTrendDetails" component={SellBoxTrendDetails} />
        </Stack.Navigator>
    )
}

const HomeNavigation = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }} initialRouteName={"Home"}  >
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="SellBoxDetails" component={SellBoxDetails} />
        </Stack.Navigator>
    )
}


const TrendingNewsNavigation = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }} initialRouteName={"Trending"}  >
            <Stack.Screen name="Trending" component={TrendingNews} />
            <Stack.Screen name="SellBoxTrendDetails" component={SellBoxTrendDetails} />
        </Stack.Navigator>
    )
}




const TabNavigatorDeepsense = () => {
    return (
        <Tab.Navigator
            initialRouteName="What's New"
            tabBarOptions={{
                activeTintColor: '#17a2b8',
                inactiveTintColor: Colors.buttonColor,
                labelStyle: {
                    fontSize: responsiveFontSize(1.3),
                    fontFamily: Fonts.Roboto.Roboto_Bold
                },
                style: { paddingVertical: responsiveWidth(1), color: 'red', backgroundColor: '#f5f5f5', width: responsiveWidth(100), },
            }}
        >
            <Tab.Screen
                name="Home"
                component={HomeNavigation}
                options={{
                    tabBarLabel: "Products",
                    tabBarIcon: ({ color, size }) => (
                        <Image source={Images.bottomLogo} tintColor={color} style={{
                            width: responsiveWidth(10), marginTop: responsiveWidth(2), height: responsiveHeight(6)
                        }} />
                    ),
                }}
            />
            <Tab.Screen
                name="Trending"
                component={TrendingNewsNavigation}
                options={{
                    tabBarLabel: "Trending",
                    tabBarIcon: ({ color, size }) => (
                        // console.log('Trending Color', color),
                        <Image source={Images.trend} tintColor={color} style={{ width: responsiveWidth(6.5), height: responsiveWidth(6.5) }} />
                    ),
                }}
            />
            <Tab.Screen
                name="Search"
                component={SellBoxSearchNavigation}
                options={{
                    tabBarLabel: 'Search',
                    tabBarIcon: ({ color, size }) => (

                        <Image source={Images.Search} tintColor={color} style={{ width: responsiveWidth(8), height: responsiveWidth(7) }} />
                    ),
                }}
            />

            <Tab.Screen
                name="Dashboard"
                component={Dashboard}
                options={{
                    tabBarLabel: 'Dashboard',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="speedometer" color={color} size={size} />
                    ),
                }}
            />
        </Tab.Navigator>
    )
}

class Routes extends React.Component {
    _isMounted = false;
    constructor() {
        super();
        this.state = {
            loading: false,
            loginuser: '',
        }
    }
    destroyLoader = () => {
        setTimeout(() => this.setState({
            loading: false
        }), 2000);
    }
    showLoader = () => {
        this.setState({
            loading: true
        });
    }
    componentDidMount() {
        this._isMounted = true;
        this.getData();
    }

    getData = async () => {
        try {
            const getToken = await AsyncStorage.getItem('UserToken');
            console.log("Token----- ", getToken);
            if (getToken !== null && getToken != undefined && getToken != '') {
                // this.loginUserDeepsense(getToken);

                this.setState({ loginuser: true });
                this.props.setLogINUser(false);
                this.props.setCheckUserLogin(false);

            } else {
                this.setState({ loginuser: true });
                this.props.setLogINUser(false);
                this.props.setCheckUserLogin(false);
            }
        } catch (e) {
            this.props.setLogINUser(false);
            this.props.setCheckUserLogin(false);
            // error reading value
        }
    }

    loginUserDeepsense = async (Token) => {
        this.showLoader();
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        myHeaders.append('Authorization', 'Bearer ' + Token);
        await fetch(ServerConstant.serverPath + "/authenticateUserFromToken", {
            method: 'POST',
            headers: myHeaders,
        }).then((response) => response.json())
            .then((result) => {
                console.log("result----", result);
                if (this._isMounted) {
                    if (result.status == 1) {
                        this.setState({ loginuser: true })
                        this.props.setLogINUser(true);
                        this.props.setCheckUserLogin(true);
                    } else if (result.status == 0) {
                        this.props.setLogINUser(false)
                        this.props.setCheckUserLogin(false);
                    } else {
                        this.props.setLogINUser(false);
                        this.props.setCheckUserLogin(false);
                    }
                    this.destroyLoader();
                }
            })
            .catch((error) => {
                console.log("result----", error);
                this.destroyLoader();
            });
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    render() {
        return (
            <>
                {this.props.checkuserlogin !== '' ? (
                    <>
                        <Loader
                            loading={this.state.loading} />
                        < NavigationContainer theme={DefaultTheme}>
                            {this.props.loginuser == false ? (
                                <LogInSellBox></LogInSellBox>
                            ) : (
                                    <StackNavigationSellBoxMain></StackNavigationSellBoxMain>
                                )
                            }
                        </ NavigationContainer></>)
                    : <Loader
                        loading={this.state.loading} />
                }
            </>

        )
    }


}
const mapStateToProps = state => {
    return {
        loginuser: state.loginuser,
        checkuserlogin: state.checkuserlogin,

    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLogINUser: data => {
            dispatch(setLogINUser(data));
        },
        setCheckUserLogin: data => {
            dispatch(setCheckUserLogin(data));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Routes);