export const Images = {
    login_bg: require('../../assets/images/login_bg.jpg'),
    CloseIcon: require('../../assets/images/CheckHeader.png'),
    settings: require('../../assets/images/settings.png'),
    loading: require('../../assets/images/loading.gif'),
    news: require('../../assets/images/news.png'),
    Dashboard: require('../../assets/images/Dashboard.png'),
    Search: require('../../assets/images/Search.png'),
    AppSetting: require('../../assets/images/SettingFooter.png'),
    checkbox: require('../../assets/images/checkbox.png'),
    checkboxchecked: require('../../assets/images/checkboxchecked.png'),
    trend: require('../../assets/images/trend.png'),
    bottomLogo: require('../../assets/images/printArticle.png'),
    menu: require('../../assets/images/menu.png'),

    sellBox:require('../../assets/images/SellBox.png')
}