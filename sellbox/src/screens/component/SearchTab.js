import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, SafeAreaView, ActivityIndicator, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize, responsiveScreenHeight } from 'react-native-responsive-dimensions';
import { Fonts, Colors, Images } from '../../styles/Index'
import DateTimePicker from '@react-native-community/datetimepicker';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { CommonFunctions, ServerConstant, Loader } from '../commoncomponent/index';
import RenderSellBoxComponent from './RenderSellBoxComponent'
import Toast from 'react-native-simple-toast';
import Share from 'react-native-share'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
export default class SearchTab extends Component {
    constructor() {
        super();
        this.state = {
            startDate: new Date(new Date().setDate(new Date().getDate() - 1)).toLocaleDateString(),
            startDateShow: false,
            endDateShow: false,
            endDate: new Date().toLocaleDateString(),
            newsList: [],
            loading: false,
            token: '',
            totalNews: 0,
            Keyword: '',

            recievedTotalNews: 0,
            isLoading: false,
            countSellBoxItem: [],
            refresh: false,
            scrollEndValue: '',
            searchAPICall: false
        }
    }

    destroyLoader = () => {
        setTimeout(() => this.setState({
            loading: false
        }), 2000);
    }
    showLoader = () => {
        this.setState({
            loading: true
        });
    }



    showStartDate = () => {
        this.setState({ startDateShow: true })
    }
    showEndDate = () => {
        this.setState({ endDateShow: true })
    }
    onEndDateChange = (event, selectedDate) => {
        console.log(selectedDate);
        if (selectedDate != undefined) {
            const currentDate = new Date(selectedDate).toLocaleDateString();
            console.log(currentDate);
            //   CommonFunctions.changeDateFormat(currentDate);
            this.setState({ endDate: currentDate, endDateShow: false })
        } else {
            this.setState({ endDateShow: false });
        }
    };
    onStartDateChange = (event, selectedDate) => {
        console.log(selectedDate);
        if (selectedDate != undefined) {
            const currentDate = new Date(selectedDate).toLocaleDateString();
            console.log(currentDate);
            this.setState({ startDate: currentDate, startDateShow: false })
        } else {
            this.setState({ startDateShow: false });
        }
    };
    componentDidMount() {
        console.log(ServerConstant.previousMinistryName !== ServerConstant.currentMinistryName);
        this.props.navigation.addListener('focus', async () => {
            // this.setState({ recievedTotalNews: 0, totalNews: 0, newsList: [] }, () => { this.getToken() })
            if (ServerConstant.previousMinistryName !== ServerConstant.currentMinistryName) {
                this.setState({ Keyword: '', loading: true })
                ServerConstant.previousMinistryName = ServerConstant.currentMinistryName;
                this.setState({ recievedTotalNews: 0, totalNews: 0, newsList: [] }, () => { this.getToken() })
            } else {
                if (!ServerConstant.searchScreenVisited) {
                    this.setState({ Keyword: '', loading: true })
                    ServerConstant.searchScreenVisited = true;
                    ServerConstant.previousMinistryName = ServerConstant.currentMinistryName;
                    this.setState({ recievedTotalNews: 0, totalNews: 0, newsList: [] }, () => { this.getToken() })
                }
            }
        });

        // this.getToken();
    }

    getToken = async () => {
        try {
            this.showLoader();
            const getToken = await AsyncStorage.getItem('UserToken');
            if (getToken !== null && getToken != undefined && getToken != '') {
                this.setState({ token: getToken });
                console.log(getToken);
                this.fetchDataByQuery(getToken, this.state.startDate, this.state.endDate);
            } else {
                this.destroyLoader();
            }
        } catch (e) {
            // error reading value
        }
    }

    fetchDataByQuery = async (Token, StartDate, EndDate) => {
        var urlencoded = new URLSearchParams();
        urlencoded.append('startDate', StartDate);
        urlencoded.append('endDate', EndDate);
        urlencoded.append("Keyword", this.state.Keyword);
        urlencoded.append('startFrom', this.state.recievedTotalNews);
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        myHeaders.append('Authorization', 'Bearer ' + Token);
        if (this.state.recievedTotalNews == 0) {
            this.showLoader();
        } else {
            this.setState({ isLoading: true })
        }
        console.log("URL", urlencoded);

        await fetch(ServerConstant.serverPath + "/seller-box/advancedSearch", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                this.destroyLoader();
                this.setState({ countSellBoxItem: [] });
                console.log("fetchByQuery", result)
                if (result.status == 1) {
                    let countdata = this.state.recievedTotalNews;
                    let data = [...this.state.newsList];
                    if (result.result.esdata.length !== 0) {
                        var concatData = data.concat(result.result.esdata);
                        var Addcountdata = countdata + result.result.esdata.length;
                        this.setState({ totalNews: result.result.count, isLoading: false, newsList: concatData, recievedTotalNews: Addcountdata });
                    } else {
                        console.log("Reached Else Part");
                        this.setState({ scrollEndValue: "No more result", isLoading: false, searchAPICall: false });
                    }


                } else if (result.status == 0) {
                } else {
                }
            }).catch((error) => {
                this.destroyLoader();
            });
    }
    render() {
        const { startDate, Keyword, endDate, startDateShow, searchAPICall, endDateShow, newsList, token, totalNews } = this.state;
        return (
            <View style={{ height: responsiveScreenHeight(93) }}>
                <Loader
                    loading={this.state.loading} />
                <SafeAreaView style={styles.safeAreaView}>
                    <View />
                    <View style={{ paddingLeft: responsiveWidth(3) }}>
                        <Text style={{
                            color: Colors.AppBasicColor, fontFamily: Fonts.Roboto.Roboto_Medium,
                            fontSize: responsiveFontSize(2)
                        }}>Search SellBox Products</Text>
                    </View>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Settings') }}>
                        <View>
                            <Image source={Images.menu} tintColor={Colors.buttonColor} style={{ width: responsiveWidth(5), height: responsiveWidth(5) }}></Image>
                        </View>
                    </TouchableOpacity>
                </SafeAreaView>
                <View style={styles.ViewDate}>
                    <View style={{ width: responsiveWidth(50), flexDirection: 'row', paddingLeft: responsiveWidth(0.4) }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={[styles.dateStyle, { paddingLeft: responsiveWidth(0.4), color: 'rgba(117, 204, 226, 1)', }]}>From : </Text>
                        </View>
                        <TouchableOpacity onPress={() => { this.showStartDate() }}>
                            <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(2) }}>
                                <View>
                                    <MaterialIcons name='date-range' size={responsiveWidth(6)} style={{ color: '#27baba' }}></MaterialIcons>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: responsiveWidth(2) }}>
                                    <Text style={styles.dateStyle}>{CommonFunctions.changeDateFormat(startDate)}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        {startDateShow ?
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={new Date()}
                                mode="date"
                                is24Hour={true}
                                display="default"
                                onChange={this.onStartDateChange}
                            /> : null}
                    </View>
                    <View style={{ flexDirection: 'row', borderLeftColor: '#bebebe', paddingLeft: responsiveWidth(2), borderLeftWidth: responsiveWidth(0.2), width: responsiveWidth(42.6), }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={[styles.dateStyle, { paddingLeft: responsiveWidth(0.4), color: 'rgba(117, 204, 226, 1)', }]}>To : </Text>
                        </View>
                        <TouchableOpacity onPress={() => { this.showEndDate() }}>
                            <View style={{ flexDirection: 'row', paddingLeft: responsiveWidth(3) }}>
                                <View>
                                    <MaterialIcons name='date-range' size={responsiveWidth(6)} style={{ color: '#27baba' }}></MaterialIcons>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: responsiveWidth(2) }}>
                                    <Text style={styles.dateStyle}>{CommonFunctions.changeDateFormat(endDate)}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        {endDateShow ?
                            <DateTimePicker
                                testID="endDate"
                                value={new Date()}
                                mode="date"
                                display="default"
                                onTouchCancel={() => { this.setState({ endDateShow: false }) }}
                                onChange={this.onEndDateChange}
                            />
                            : null}
                    </View>
                </View>
                <View style={[styles.ViewDate, { backgroundColor: '#F4F6F6' }]}>
                    <View style={{ marginRight: responsiveWidth(4) }}>
                        <TextInput
                            placeholder={'Search News'}
                            value={this.state.Keyword}
                            name={'Keyword'}
                            testID={'Keyword'}
                            onChangeText={(Keyword) => this.setState({ Keyword })}
                            style={{ backgroundColor: '#ffff', paddingHorizontal: responsiveWidth(2), paddingVertical: responsiveWidth(1), borderRadius: responsiveWidth(1), height: responsiveHeight(4), width: responsiveWidth(70) }}>
                        </TextInput>
                    </View>
                    <View>

                        <TouchableOpacity onPress={() => {
                            this.setState({ recievedTotalNews: 0, searchAPICall: true, totalNews: 0, newsList: [] }, () => { if (!this.state.loading) { this.fetchDataByQuery(token, startDate, endDate) } })
                        }}>


                            <View style={{
                                borderRadius: responsiveWidth(2),
                                width: responsiveWidth(20), justifyContent: 'center', alignItems: 'center',
                                height: responsiveHeight(4), backgroundColor: Colors.buttonColor   //'#3DE4B2'
                            }}>
                                <Text style={[styles.dateStyle, { color: '#ffff' }]}>Search</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                { newsList.length == 0 && !this.state.loading &&
                    <View style={styles.ViewDate}>
                        <Text style={[styles.dateStyle, { fontFamily: Fonts.Roboto.Roboto_Light }]}>No news found. You're all caught up.</Text>
                    </View>
                }

                <FlatList
                    data={newsList}
                    onEndReached={this.handleLoadMoreData}
                    onEndReachedThreshold={200}
                    renderItem={({ item, index }) => (
                        <RenderSellBoxComponent index={index} item={item} navigation={this.props}
                            getNews={this.getNews}
                            countSellBoxItem={this.state.countSellBoxItem}
                        />
                    )}
                    keyExtractor={(item, index) => item._id.toString()}
                    ListFooterComponent={this.renderFooter}
                    extraData={this.state.refresh}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', top: responsiveHeight(79), right: 50 }}>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { this.DownloadNewsPostAPi(this.state.token, this.state.countSellBoxItem, 'EMAIL') }}>
                        <View style={styles.socialViewStyle}>
                            <Text style={styles.badgetText}>{this.state.countSellBoxItem.length !== 0 ? this.state.countSellBoxItem.length : ''}</Text>
                            <MaterialCommunityIcons color={'#fff'} size={responsiveWidth(6.6)} name={'email'}></MaterialCommunityIcons>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { this.DownloadNewsPostAPi(this.state.token, this.state.countSellBoxItem, 'WHATSAPP') }}>
                        <View style={[styles.socialViewStyle, { marginLeft: responsiveWidth(8) }]}>
                            <Text style={styles.badgetText}>{this.state.countSellBoxItem.length !== 0 ? this.state.countSellBoxItem.length : ''}</Text>
                            <MaterialCommunityIcons color={'#fff'} size={responsiveWidth(7.6)} name={'whatsapp'}></MaterialCommunityIcons>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }


    DownloadNewsPostAPi = async (Token, checkBoxIDs, type) => {
        console.log("New List" + checkBoxIDs);
        if (checkBoxIDs.length > 0) {
            var userData = {
                // "Media": ['Print'],
                "Type": 'ADVANCED',   // 'ADVANCED',
                "SelectedRecords": checkBoxIDs,
            }
        } else {
            return Toast.show('Please select the news', Toast.LONG,);
        }
        var myHeaders = new Headers();
        this.showLoader();
        // console.log(Token);
        myHeaders.append('Content-Type', 'application/json');
        myHeaders.append('Authorization', 'Bearer ' + this.state.token);
        console.log(userData, myHeaders);
        await fetch(ServerConstant.serverPath + "/dataSearch/export/PDF", {   ///api/sendNewsOnEmail
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(userData),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                console.log(result);
                if (result.status == 1) {
                    this.setState({ countSellBoxItem: [] });
                    // const ministryName = result.result.MinistriesName;
                    if (type == 'EMAIL') {
                        this.convertPdfWurlBase64(result.result.filePath, type);
                    } else {
                        this.convertPdfWurlBase64(result.result.filePath, type);
                    }
                } else if (result.status == 0) {
                    Toast.show(result.description, Toast.LONG,);
                } else {
                    Toast.show(result.description, Toast.LONG,);
                }
                this.destroyLoader();
            }).catch(err => {
                alert(err);
                this.destroyLoader();
            })
    }
    convertPdfWurlBase64 = async (url, type, ministryName) => {
        await CommonFunctions.getBase64(url).then((result) => {
            this.ShareProducts(result, type, ministryName);
        }).catch((err) => {

        })
    }


    ShareProducts = async (pdfUrl, type) => {

        const whatMessage = "Sell Box Analysis " + '\n\n'
            + '*Get Inside the Box*🛒 \n\n'
            + 'Increase your online sales by getting inside ' + '*Sell Box*' + '\n\n'
            + '' + 'Visit@ ' + 'https://sellbox.in' + '\n\n';


        const otherSharing = "Sell Box Analysis " + '\n\n'
            + 'Get Inside the Box 🛒 \n\n'
            + 'Increase your online sales by getting inside ' + 'Sell Box' + '\n\n'
            + '' + 'Visit@ ' + 'https://sellbox.in' + '\n\n';


        const shareOptions = {
            title: 'SellBox',
            message: type == 'WhatsApp' ? whatMessage : otherSharing,
            subject: 'SellBox Products',
            urls: ['data:application/pdf;base64,' + pdfUrl],
            social: type == 'EMAIL' ? Share.Social.EMAIL : Share.Social.WHATSAPP,
        };
        try {
            const data = Share.shareSingle(shareOptions).then(res => console.log(res)).catch(err => console.log(err));
        } catch (err) {
            console.log(err);
        }
    }
    getNews = (newsID) => {
        console.log(newsID)
        this.setState({
            refresh: !this.state.refresh
        })
        if (this.state.countSellBoxItem.includes(newsID)) {
            var idx = this.state.countSellBoxItem.indexOf(newsID);
            if (idx != -1) {
                this.state.countSellBoxItem.splice(idx, 1);
            }
        } else {
            this.state.countSellBoxItem.push(newsID);
        }
    }


    handleLoadMoreData = () => {
        console.log(this.state.recievedTotalNews);
        if (this.state.totalNews !== this.state.recievedTotalNews && !this.state.isLoading && this.state.scrollEndValue == '') {
            console.log("API Call");
            this.fetchDataByQuery(this.state.token, this.state.startDate, this.state.endDate);
        } else {
            console.log("API NOT Call");
        }
    }

    renderFooter = () => {
        return (
            <>
                {this.state.isLoading ?
                    <View style={styles.loader}>
                        <ActivityIndicator size={'large'} color={'#27baba'} ></ActivityIndicator>
                    </View> : null}
                {
                    this.state.totalNews !== 0 && this.state.totalNews == this.state.recievedTotalNews && !this.state.isLoading ?
                        <View style={{
                            borderTopWidth: responsiveWidth(0.1),
                            width: responsiveWidth(100), backgroundColor: '#fff',
                            alignContent: 'center', alignItems: 'center', justifyContent: 'center',
                            marginBottom: responsiveWidth(2)
                        }}>
                            <Text style={{
                                color: '#27baba',
                                paddingVertical: responsiveWidth(4),
                                fontFamily: Fonts.Roboto.Roboto_Bold,
                                fontSize: responsiveFontSize(1.8)
                            }}>No more news found</Text>
                        </View>
                        : null
                }
            </>
        )
    }




}

const styles = StyleSheet.create({
    flex: 1,
    loader: {
        paddingVertical: responsiveWidth(5),
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    safeAreaView: {

        paddingHorizontal: responsiveWidth(3),
        height: responsiveHeight(7),
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#27baba',
        flexDirection: 'row'
        // width: responsiveWidth(100),
        // paddingTop: responsiveWidth(2),
        // paddingHorizontal: responsiveWidth(4),
        // paddingBottom: responsiveWidth(1.5),
        // backgroundColor: '#27baba'
    },
    ViewDate: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    dateStyle: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Medium
    },
    socialViewStyle: {
        backgroundColor: '#27baba', justifyContent: 'center',
        alignItems: 'center', borderRadius: responsiveWidth(6), width: responsiveWidth(10), height: responsiveWidth(10)
    },
    badgetText: {
        fontFamily: Fonts.Roboto.Roboto_Bold, position: 'absolute', bottom: responsiveWidth(8)
    }
})