import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize, responsiveScreenWidth } from 'react-native-responsive-dimensions';
import { Fonts, Colors, Images } from '../../styles/Index'
import TrendingTab from '../trendsComponent/TrendingTab'
export default class TrendingNews extends Component {
    constructor() {
        super();
        this.state = {
        }
    }
    render() {
        return (
            <>
                <SafeAreaView style={styles.safeAreaView}>
                    <View>
                    </View>
                    <View style={{ paddingLeft: responsiveWidth(4) }}>
                        <Text style={{
                            color: Colors.AppBasicColor,
                            fontFamily: Fonts.Roboto.Roboto_Medium,
                            fontSize: responsiveFontSize(2)
                        }}>SellBox Trending</Text>
                    </View>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Settings') }}>
                        <View>
                            <Image source={Images.menu} tintColor={Colors.buttonColor} style={{ width: responsiveWidth(5), height: responsiveWidth(5) }}></Image>
                        </View>
                    </TouchableOpacity>

                </SafeAreaView>
                <TrendingTab navigation={this.props.navigation}>
                </TrendingTab>
            </>
        );
    }
}
const styles = StyleSheet.create({
    flex: 1,
    safeAreaView: {
        paddingHorizontal: responsiveWidth(3),
        height: responsiveHeight(7),
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#27baba',
        flexDirection: 'row'
    },
})

