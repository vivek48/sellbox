import React, { Component } from 'react';
import { View, Text, Animated, Image, StyleSheet, Platform, SafeAreaView, ActivityIndicator, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight, useResponsiveScreenWidth, responsiveScreenHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { CommonFunctions, ServerConstant, Loader } from '../commoncomponent/index';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen'
import RenderSellBoxComponent from './RenderSellBoxComponent'
import { setLogINUser, setCheckUserLogin, setDATA_INFO } from "../../stores/actions";
import { connect } from 'react-redux';
import reducer from '../../stores/reducer';
import Toast from 'react-native-simple-toast';
import { Picker } from '@react-native-community/picker';
import BottomSheetShare from './BottomSheetShare';
import Share from 'react-native-share'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import RNFetchBlob from 'rn-fetch-blob'
class Home extends Component {
    _isMounted = false;
    constructor() {
        super();
        this.state = {
            newsList: [],
            loading: false,
            totalItem: 0,
            languageList: [],
            selectedLanguage: '',
            SiteName: '',
            token: '',
            recievedTotalNews: 0,
            isLoading: false,
            // language:'Java',
            CurrentSiteID: '',
            CurrentSiteName: '',
            ministryList: [],
            CurrentOrganizationID: '',

            countSellBoxItem: [],
            refresh: false,
            scrollEndValue: '',
            checkDefaultCheckboxValue: false,
        }
    }
    destroyLoader = () => {
        setTimeout(() => this.setState({
            loading: false
        }), 2000);
    }
    showLoader = () => {
        this.setState({
            loading: true
        });

    }



    componentDidMount() {
        SplashScreen.hide();
        this.setState({ recievedTotalNews: 0, totalItem: 0, newsList: [] }, () => { this.getToken() })
    }
    getToken = async () => {
        console.log("hhdhhdjjh");
        try {
            this.showLoader();
            const getToken = await AsyncStorage.getItem('UserToken');

            if (getToken !== null && getToken != undefined && getToken != '') {
                console.log(getToken);
                this.fetchDataByDate(getToken);
                this.setState({ token: getToken });
            } else {
                this.destroyLoader();
            }
        } catch (e) {
            this.destroyLoader();
        }
    }
    fetchDataByDate = async (Token) => {
        var today = new Date();
        var convertEndDate = today.toLocaleDateString();
        var yesterday = new Date(today.setDate(today.getDate() - 1));
        var convertStartDate = yesterday.toLocaleDateString();
        var urlencoded = new URLSearchParams();
        urlencoded.append('startDate', convertStartDate);
        urlencoded.append('endDate', convertEndDate);
        urlencoded.append('startFrom', this.state.recievedTotalNews);
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        myHeaders.append('Authorization', 'Bearer ' + Token);
        if (this.state.recievedTotalNews == 0) {
            this.showLoader();
        } else {
            this.setState({ isLoading: true })
        }
        await fetch(ServerConstant.serverPath + "/seller-box/advancedSearch", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                this.destroyLoader();
                if (this.state.loading) {
                    if (this.state.ministryList.length == '0') {
                        this.fetchDataUserInfo(Token);
                    }
                }

                if (result.status == 1) {
                    // console.log((result.result.aggregations));
                    let countdata = this.state.recievedTotalNews;
                    let data = [...this.state.newsList];
                    if (result.result.esdata.length !== 0) {
                        var concatData = data.concat(result.result.esdata);
                        var Addcountdata = countdata + result.result.esdata.length;
                        console.log("RECIIDI", Addcountdata);
                        console.log(result.result.esdata.length);
                        this.setState({ totalItem: result.result.count, isLoading: false, newsList: concatData, recievedTotalNews: Addcountdata });
                    } else {
                        this.setState({ scrollEndValue: "No more result", isLoading: false, });
                    }

                } else if (result.status == 0) {
                } else {
                }
            }).catch((error) => {
                this.destroyLoader();
            });
    }

    fetchLaunguage = async () => {

        var urlencoded = new URLSearchParams();
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        myHeaders.append('Authorization', 'Bearer ' + this.state.token);
        this.showLoader();
        await fetch(ServerConstant.serverPath + "/deepsense/language", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                this.destroyLoader();
                if (result.status == 1) {
                    this.setState({ languageList: result.result.Languages, selectedLanguage: result.result.UserLanguage, checkModelOpen: true, });
                } else if (result.status == 0) {
                } else {
                }
            }).catch((error) => {
                this.destroyLoader();
            });
    }


    fetchDataUserInfo = async (Token) => {
        var urlencoded = new URLSearchParams();
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        myHeaders.append('Authorization', 'Bearer ' + Token);
        this.showLoader();
        await fetch(ServerConstant.serverPath + "/me/info", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                console.log("Result Info---", result);
                this.destroyLoader();
                if (result.status == 1) {
                    ServerConstant.currentMinistryName = result.result.CurrentSiteID;
                    ServerConstant.previousMinistryName = result.result.CurrentSiteID;
                    this.setState({
                        CurrentSiteID: result.result.CurrentSiteID,
                        CurrentSiteName: result.result.CurrentSiteName,
                        ministryList: result.result.OrgSites,
                        CurrentOrganizationID: result.result.CurrentOrganizationID,
                    })
                    this.fetchLaunguage();
                    this.props.setDATA_INFO(result.result)
                } else if (result.status == 0) {
                } else {
                }
            }).catch((error) => {
                this.destroyLoader();
            });
    }




    updateSessionSave = async (CurrentSiteID) => {
        var urlencoded = new URLSearchParams();
        urlencoded.append('OrgID', this.state.CurrentOrganizationID);
        urlencoded.append('SiteID', CurrentSiteID);
        urlencoded.append('Language', this.state.selectedLanguage);
        urlencoded.append('IsDefault', this.state.checkDefaultCheckboxValue);
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        myHeaders.append('Authorization', 'Bearer ' + this.state.token);
        this.showLoader();
        console.log(urlencoded);
        await fetch(ServerConstant.serverPath + "/me/updateSession", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                // console.log(result);
                if (result.status == 1) {
                    // this.setState({ CurrentSiteID: CurrentSiteID, });
                    this.setState({ recievedTotalNews: 0, CurrentSiteID: CurrentSiteID, totalItem: 0, countSellBoxItem: [], newsList: [] }, () => { this.fetchDataByDate(this.state.token) })
                } else if (result.status == 0) {
                    this.destroyLoader();
                } else {
                    this.destroyLoader();
                }
            }).catch((error) => {
                this.destroyLoader();
            });
    }





    DownloadNewsPostAPi = async (Token, checkBoxIDs, type) => {
        console.log("New List", checkBoxIDs, type);
        if (checkBoxIDs.length > 0) {
            var userData = {
                "Type": 'ADVANCED',   // 'ADVANCED',
                "SelectedRecords": checkBoxIDs,
            }
        } else {
            return Toast.show('Please select the news', Toast.LONG,);
        }
        var myHeaders = new Headers();
        this.showLoader();
        console.log(Token);
        myHeaders.append('Content-Type', 'application/json');
        myHeaders.append('Authorization', 'Bearer ' + Token);
        await fetch(ServerConstant.serverPath + "/dataSearch/export/PDF", {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(userData),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                console.log(result);
                if (result.status == 1) {
                    this.setState({ countSellBoxItem: [] });
                    if (type == 'EMAIL') {
                        this.convertPdfWurlBase64(result.result.filePath, type);
                    } else {
                        this.convertPdfWurlBase64(result.result.filePath, type);
                    }
                } else if (result.status == 0) {
                    Toast.show(result.description, Toast.LONG,);
                } else {
                    Toast.show(result.description, Toast.LONG,);
                }
                this.destroyLoader();
            }).catch(err => {
                alert(err)
                // console.log(err);
                this.destroyLoader();
                // Toast.show(err, Toast.LONG,);

            })
    }


    convertPdfWurlBase64 = async (url, type) => {
        await CommonFunctions.getBase64(url).then((result) => {
            this.ShareProducts(result, type)
        }).catch((err) => {

        })
    }
    render() {
        const { newsList, totalItem, ministryList, languageList, CurrentSiteID, CurrentRoleName, selectedLanguage, AssignedOrgs
            , CurrentOrganizationID, checkModelOpen, token, SiteName, } = this.state;
        const { navigation } = this.props;
        return (
            <View style={{ height: responsiveScreenHeight(93), }}>
                <Loader
                    loading={this.state.loading} />
                <SafeAreaView style={styles.safeAreaView}>
                    <View />
                    <View>
                        <View style={{ paddingLeft: responsiveWidth(5) }}>
                            <Text style={{
                                color: Colors.AppBasicColor,
                                textAlign: 'center',
                                justifyContent: 'center',
                                fontSize: responsiveFontSize(2.4), fontFamily: Fonts.Roboto.Roboto_Regular
                            }}>
                                Greetings!</Text>
                        </View>
                        <View>
                            <Text style={{
                                color: Colors.AppBasicColor,
                                fontSize: responsiveFontSize(2),
                                fontFamily: Fonts.FontAwe.fontawesome_webfont
                            }}>
                                You have {totalItem} unread stories </Text>
                        </View>

                    </View>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Settings') }}>
                        <View >
                            <Image source={Images.menu} tintColor={Colors.buttonColor} style={{ width: responsiveWidth(5), height: responsiveWidth(5) }}></Image>
                        </View>
                    </TouchableOpacity>
                </SafeAreaView>
                <SafeAreaView style={styles.safeAreaViewSecond}>
                    <View>
                        {ministryList.length > 0 &&
                            < Picker
                                enabled={true}
                                mode={'dropdown'}
                                selectedValue={CurrentSiteID}
                                style={{
                                    height: responsiveWidth(6),
                                    width: responsiveWidth(98),
                                }}
                                onValueChange={(item, Index) => {

                                    ServerConstant.currentMinistryName = item;
                                    this.setState({ CurrentSiteID: item });
                                    console.log(item);
                                    this.updateSessionSave(item)
                                }}>
                                {
                                    ministryList.map((item, index) => (
                                        <Picker.Item key={index} label={item.SiteName} value={item.SiteID} />
                                    ))
                                }
                            </Picker>
                        }
                    </View>
                </SafeAreaView>
                { newsList.length == 0 && !this.state.loading &&
                    <View style={styles.ViewDate}>
                        <Text style={[styles.dateStyle, { textAlign: 'center', fontFamily: Fonts.Roboto.Roboto_Light }]}>No new item found.
                         You are all caught up.</Text>
                    </View>
                }
                <FlatList
                    data={this.state.newsList}
                    onEndReached={this.handleLoadMoreData}
                    onEndReachedThreshold={200}
                    renderItem={({ item, index, }) => (
                        <RenderSellBoxComponent index={index} item={item} navigation={this.props}
                            getNews={this.getNews}
                            countSellBoxItem={this.state.countSellBoxItem}
                        />
                    )}
                    keyExtractor={(item, index) => item._id.toString()}
                    ListFooterComponent={this.renderFooter}
                    extraData={this.state.refresh}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', top: responsiveHeight(79), right: 50 }}>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { this.DownloadNewsPostAPi(this.state.token, this.state.countSellBoxItem, 'EMAIL') }}>
                        <View style={styles.socialViewStyle}>
                            <Text style={styles.badgetText}>{this.state.countSellBoxItem.length !== 0 ? this.state.countSellBoxItem.length : ''}</Text>
                            <MaterialCommunityIcons color={'#fff'} size={responsiveWidth(6.6)} name={'email'}></MaterialCommunityIcons>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { this.DownloadNewsPostAPi(this.state.token, this.state.countSellBoxItem, 'WHATSAPP') }}>

                        <View style={[styles.socialViewStyle, { marginLeft: responsiveWidth(8) }]}>
                            <Text style={styles.badgetText}>{this.state.countSellBoxItem.length !== 0 ? this.state.countSellBoxItem.length : ''}</Text>
                            <MaterialCommunityIcons color={'#fff'} size={responsiveWidth(7.6)} name={'whatsapp'}></MaterialCommunityIcons>
                        </View>
                    </TouchableOpacity>
                </View>
            </View >
        );
    }
    ShareProducts = async (pdfUrl, type) => {

        const whatMessage = "Sell Box Analysis " + '\n\n'
            + '*Get Inside the Box*🛒 \n\n'
            + 'Increase your online sales by getting inside ' + '*Sell Box*' + '\n\n'
            + '' + 'Visit@ ' + 'https://sellbox.in' + '\n\n';


        const otherSharing = "Sell Box Analysis " + '\n\n'
            + 'Get Inside the Box 🛒 \n\n'
            + 'Increase your online sales by getting inside ' + 'Sell Box' + '\n\n'
            + '' + 'Visit@ ' + 'https://sellbox.in' + '\n\n';


        const shareOptions = {
            title: 'SellBox',
            message: type == 'WhatsApp' ? whatMessage : otherSharing,
            subject: 'SellBox Products',
            urls: ['data:application/pdf;base64,' + pdfUrl],
            social: type == 'EMAIL' ? Share.Social.EMAIL : Share.Social.WHATSAPP,
        };
        try {
            const data = Share.shareSingle(shareOptions).then(res => console.log(res)).catch(err => console.log(err));
        } catch (err) {
            console.log(err);
        }
    }





    getNews = (newsID) => {
        console.log("Selcted new id", newsID)
        this.setState({
            refresh: !this.state.refresh
        })
        if (this.state.countSellBoxItem.includes(newsID)) {
            var idx = this.state.countSellBoxItem.indexOf(newsID);
            if (idx != -1) {
                this.state.countSellBoxItem.splice(idx, 1);
            }
        } else {
            this.state.countSellBoxItem.push(newsID);
        }
    }





    handleLoadMoreData = () => {
        console.log(this.state.recievedTotalNews, "this.state.totalNews" + this.state.totalItem);
        if (this.state.totalItem !== this.state.recievedTotalNews && !this.state.isLoading && this.state.scrollEndValue == '') {
            console.log("API Call");
            this.fetchDataByDate(this.state.token);
        } else {
            console.log("API NOT Call");
        }
    }

    renderFooter = () => {
        return (
            <>
                {this.state.isLoading ?
                    <View style={styles.loader}>
                        <ActivityIndicator size={'large'} color={'#27baba'} ></ActivityIndicator>
                    </View> : null}
                {
                    this.state.totalItem !== 0 && this.state.totalItem == this.state.recievedTotalNews && !this.state.isLoading ?
                        <View style={{
                            borderTopWidth: responsiveWidth(0.1),
                            width: responsiveWidth(100), backgroundColor: '#fff',
                            alignContent: 'center', alignItems: 'center', justifyContent: 'center',
                            marginBottom: responsiveWidth(2)
                        }}>
                            <Text style={{
                                color: '#27baba',
                                paddingVertical: responsiveWidth(4),
                                fontFamily: Fonts.Roboto.Roboto_Bold,
                                fontSize: responsiveFontSize(1.8)
                            }}>No more news found</Text>
                        </View>
                        : null
                }
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        dataInfo: state.dataInfo
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setDATA_INFO: data => {
            dispatch(setDATA_INFO(data));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
const styles = StyleSheet.create({
    flex: 1,
    loader: {
        paddingVertical: responsiveWidth(5),
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    safeAreaView: {
        paddingVertical: responsiveWidth(1.5),
        paddingHorizontal: responsiveWidth(3),
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#27baba',
        flexDirection: 'row'
    },
    safeAreaViewSecond: {
        // width: responsiveWidth(100),
        flexDirection: 'row',
        paddingHorizontal: responsiveWidth(3),
        paddingVertical: responsiveWidth(2),
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    ViewDate: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    dateStyle: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Medium
    },
    socialViewStyle: {
        backgroundColor: '#27baba', justifyContent: 'center',
        alignItems: 'center', borderRadius: responsiveWidth(6), width: responsiveWidth(10), height: responsiveWidth(10)
    },
    badgetText: {
        fontFamily: Fonts.Roboto.Roboto_Bold, position: 'absolute', bottom: responsiveWidth(8)
    }
})
