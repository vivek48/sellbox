import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize, } from 'react-native-responsive-dimensions';
import { Fonts, Colors, Images } from '../../styles/Index'
import { setLogINUser, setDATA_INFO } from "../../stores/actions";
import AsyncStorage from '@react-native-community/async-storage';
import { CommonFunctions, ServerConstant, Loader } from '../commoncomponent/index';
import Icon from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import reducer from '../../stores/reducer';
import ZoomImage from '../commoncomponent/ZoomImage';
import Share from 'react-native-share'
import ImgToBase64 from 'react-native-image-base64';
import BottmSheetShare from './BottomSheetShare';

import Toast from 'react-native-simple-toast';
import RBSheet from "react-native-raw-bottom-sheet";
const url = '';
class SellBoxDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            token: '',
            sellBoxItem: this.props.route.params.sellBoxItem,
            newsSource: '',
            imageURL: [],
            // For sharing
            headerLine: '',
            ministry: '',
            typeData: '',
            publishDate: '',
            Source: '',
            RBSheetOpenCheck: false,

        }
    }
    destroyLoader = () => {
        setTimeout(() => this.setState({
            loading: false
        }), 2000);
    }
    showLoader = () => {
        this.setState({
            loading: true
        });
    }
    ShareNews = async (socialType) => {
        var imageBase64Arr = []

        console.log(this.state.sellBoxItem._source.imageUrl);
        if (this.state.sellBoxItem._source.imageUrl != '' && this.state.sellBoxItem._source.imageUrl != undefined) {
            await ImgToBase64.getBase64String(this.state.sellBoxItem._source.imageUrl)
                .then(base64String => {
                    var image64 = `data:image/jpeg;base64,` + base64String;
                    imageBase64Arr.push(image64);

                })
                .catch(err => console.log(err));
        }



        const whatMessage = "Sell Box Analysis " + '\n\n'
            + '*Get Inside the Box*🛒 \n\n'
            + 'Increase your online sales by getting inside ' + '*Sell Box*' + '\n\n'
            + '' + 'Visit@ ' + 'https://sellbox.in' + '\n\n';


        const otherSharing = "Sell Box Analysis " + '\n\n'
            + 'Get Inside the Box 🛒 \n\n'
            + 'Increase your online sales by getting inside ' + 'Sell Box' + '\n\n'
            + '' + 'Visit@ ' + 'https://sellbox.in' + '\n\n';
        var shareOptions;
        if (imageBase64Arr.length > 0) {
            shareOptions = {
                title: 'SellBox',
                message: socialType == 'WhatsApp' ? whatMessage : otherSharing,
                subject: 'SellBox Products',
                urls: imageBase64Arr,
                social: socialType == 'Email' ? Share.Social.EMAIL : socialType == 'WhatsApp' ? Share.Social.WHATSAPP : Share.Social.TWITTER
            };
        } else {
            shareOptions = {
                title: 'SellBox',
                message: socialType == 'WhatsApp' ? whatMessage : otherSharing,
                subject: 'SellBox Products',
                // urls: imageBase64Arr,
                social: socialType == 'Email' ? Share.Social.EMAIL : socialType == 'WhatsApp' ? Share.Social.WHATSAPP : Share.Social.TWITTER
            };
        }
        try {
            this.RBSheet.close();
            const data = Share.shareSingle(shareOptions).then(res => console.log("resss", res)).catch(err => {
                Toast.show("url not found", Toast.LONG,)
                console.log("err", err)
            });
        } catch (err) {
            console.log(err)
        }
    }


    typeCheck = (socialType) => {

        this.ShareNews(socialType);

    }


    render() {
        const { token, newsSource, imageURL, sellBoxItem } = this.state;
        console.log(sellBoxItem);

        return (
            <View style={{ height: responsiveHeight(91), }}>
                <Loader
                    loading={this.state.loading} />
                <SafeAreaView style={styles.safeAreaView}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate(this.props.route.params.navigationSreen) }}>
                        <View>
                            <Text style={{
                                fontSize: responsiveFontSize(2),
                                fontFamily: Fonts.Roboto.Roboto_Medium,
                                opacity: 0.7,
                                color: Colors.buttonColor
                            }}>Back</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ paddingRight: responsiveWidth(4) }}>
                        <Text style={{
                            color: Colors.AppBasicColor,
                            fontFamily: Fonts.Roboto.Roboto_Medium,
                            fontSize: responsiveFontSize(2)
                        }}>SellBox Item Details</Text>
                    </View>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Settings') }}>
                        <View>
                            <Image source={Images.menu} tintColor={Colors.buttonColor} style={{ width: responsiveWidth(5), height: responsiveWidth(5) }}></Image>
                        </View>
                    </TouchableOpacity>
                </SafeAreaView>
                <ScrollView>
                    <View>
                        <View style={styles.viewHederList}>
                            <View style={{
                                marginTop: responsiveWidth(2),
                                justifyContent: 'space-between', alignItems: 'center',
                                flexDirection: 'row', height: responsiveHeight(7),

                            }}>
                                <View style={{ alignItems: 'center', }}>
                                    <Text style={[styles.viewTextHeder, { color: '#27baba', textAlign: 'right', }]}>
                                        {sellBoxItem._source.channelSource} </Text>
                                </View>
                                <TouchableOpacity onPress={() => {
                                    this.RBSheet.open();
                                    this.setState({
                                        headerLine: newsSource.headline,
                                        ministry: newsSource.ministry,
                                        typeData: newsSource.typedata,
                                        publishDate: new Date(newsSource.publishDate).toLocaleDateString(),
                                        Source: newsSource.source,
                                        RBSheetOpenCheck: true
                                    })
                                }}>
                                    <View style={{
                                        backgroundColor: '#27baba',
                                        height: responsiveWidth(9), borderRadius: responsiveWidth(5),
                                        width: responsiveWidth(9), paddingRight: responsiveWidth(1.2),
                                        alignItems: 'center', flexDirection: 'row', justifyContent: 'center'
                                    }}>
                                        <Icon name='sharealt' size={22} style={{ opacity: 0.9 }} color={'#ffff'}></Icon>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={[styles.viewHederList, { paddingVertical: responsiveWidth(3) }]}>
                            <Text style={[styles.viewTextHeder, {
                                textAlign: 'center',
                                color: '#EAB839',
                            }]}>
                                {sellBoxItem._source.title.trim()}</Text>
                        </View>
                        <View style={[styles.viewHederList, { justifyContent: 'space-between', flexDirection: 'row' }]}>
                            <View>
                                <Text style={{
                                    color: '#212529', fontFamily: Fonts.Roboto.Roboto_Medium,
                                    fontSize: responsiveFontSize(1.7)
                                }}>Seller Name : {sellBoxItem._source.sellerName.trim()} </Text>
                            </View>
                            <View>
                                {sellBoxItem._source.sellBox &&
                                    <Text style={{
                                        textAlign: 'center', fontSize: responsiveFontSize(2.4),
                                        fontFamily: Fonts.Roboto.Roboto_Medium,
                                    }}> 🛒 </Text>
                                }
                            </View>
                        </View>
                        <View style={[styles.viewHederList, { paddingVertical: responsiveWidth(3) }]}>
                            <Text style={[styles.viewTextHeder, { textAlign: 'center', color: '#34495e' }]}>
                                {sellBoxItem._source.siteName}</Text>
                        </View>
                        {sellBoxItem._source.imageUrl !== null && sellBoxItem._source.imageUrl.trim() !== '' &&

                            <View style={{
                                borderTopWidth: responsiveWidth(0.1),
                                justifyContent: 'center', alignItems: 'center',
                                paddingVertical: responsiveWidth(2),
                                paddingBottom: responsiveWidth(5)
                            }}>
                                {/* <ZoomImage imageUri={sellBoxItem._source.imageUrl} /> */}
                                <Image source={{ uri: sellBoxItem._source.imageUrl }} resizeMode={'contain'}
                                    style={{ width: responsiveWidth(100), height: responsiveHeight(30) }}></Image>
                                {/* <Image source={{ uri: `${sellBoxItem._source.imageUrl.trim()}` }}></Image> */}
                            </View>
                        }
                    </View>
                    <View style={[styles.viewHederList, { justifyContent: 'space-between', paddingHorizontal: responsiveWidth(7), flexDirection: 'row' }]}>
                        <View>
                            <Text style={{
                                color: '#212529', fontFamily: Fonts.Roboto.Roboto_Medium,
                                fontSize: responsiveFontSize(1.8)
                            }}>Selling Price : {sellBoxItem._source.currency} {sellBoxItem._source.sp}</Text>
                        </View>

                        <View>
                            {sellBoxItem._source.sellerRating !== null && sellBoxItem._source.sellerRating !== undefined && sellBoxItem._source.sellerRating !== '' &&
                                <Text style={{
                                    color: '#212529', fontFamily: Fonts.Roboto.Roboto_Medium,
                                    fontSize: responsiveFontSize(1.8)
                                }}>Rating : {sellBoxItem._source.sellerRating}</Text>
                            }
                        </View>
                    </View>
                </ScrollView>

                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    height={responsiveHeight(20)}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "transparent",
                            width: responsiveWidth(100),
                            height: responsiveHeight(20)
                        },
                        draggableIcon: {
                            // backgroundColor: "#000"
                        }
                    }}
                >
                    <BottmSheetShare typeCheck={this.typeCheck} />
                </RBSheet>
            </View >
        );
    }
}
const mapStateToProps = state => {
    return {
        loginuser: state.loginuser,
        dataInfo: state.dataInfo,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLogINUser: data => {
            dispatch(setLogINUser(data));
        },
        setDATA_INFO: data => {
            dispatch(setDATA_INFO(data));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(SellBoxDetails);

const styles = StyleSheet.create({
    flex: 1,
    viewHederList: {
        paddingHorizontal: responsiveWidth(3)
    },
    viewTextHeder: {
        fontSize: responsiveFontSize(2), fontFamily: Fonts.Roboto.Roboto_Bold
    },
    ViewRadius1: {
        justifyContent: 'space-between', flexDirection: 'row', paddingHorizontal: responsiveWidth(4), paddingVertical: responsiveWidth(1)
    },
    ViewRadiusText1: {
        fontFamily: Fonts.Roboto.Roboto_Regular,
        fontSize: responsiveFontSize(1.8),
        color: '#212529'
    },
    safeAreaView: {
        paddingHorizontal: responsiveWidth(3),
        height: responsiveHeight(7),
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#27baba',
        flexDirection: 'row'
    },
    ViewDate: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },

})