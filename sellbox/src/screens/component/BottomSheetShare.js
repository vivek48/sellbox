import React, { Component, useEffect, useState, useRef } from 'react';
import { View, Text, Image, Button, StyleSheet, TextInput, Animated, ScrollView, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';

import RBSheet from "react-native-raw-bottom-sheet";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Images, Fonts, Colors } from '../../styles/Index'
import Share from 'react-native-share'
const BottomSheetShare = ({ closeFunction, typeCheck }, props) => {
    const refRBSheet = useRef();
    console.log(refRBSheet);
    return (
        <View>
            {/* <Button title="OPEN BOTTOM SHEET" onPress={() => refRBSheet.current.open()} /> */}
            {/* <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                clo
                height={responsiveHeight(20)}
                customStyles={{
                    wrapper: {
                        backgroundColor: "transparent",
                        width: responsiveWidth(100),
                        height: responsiveHeight(20)
                    },
                    draggableIcon: {
                        // backgroundColor: "#000"
                    }
                }}
            > */}
            <View style={{
                marginHorizontal: responsiveWidth(1),
                height: responsiveHeight(30),
                borderRadius: responsiveWidth(3),
                opacity: 0.8,
                // backgroundColor: '#27baba',
                flexDirection: 'row'
            }}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => { typeCheck("WhatsApp") }}>
                    <View style={styles.bottomSheetStyle}>
                        <View style={[styles.iconStyles, { backgroundColor: '#27baba' }]}>
                            <MaterialCommunityIcons color={'#ffff'} size={responsiveWidth(8)} name={'whatsapp'}></MaterialCommunityIcons>
                        </View>
                        <View>
                            <Text style={styles.textStyle}>WhatsApp</Text>
                        </View>

                    </View>
                </TouchableOpacity>
                {/* <TouchableOpacity activeOpacity={0.7} onPress={() => {
                    Share.isPackageInstalled('org.telegram.messenger')
                        .then((response) => {
                            console.log(response);
                            // { isInstalled: true/false, message: 'Package is Installed' }
                        })
                        .catch((error) => {
                            console.log(error);
                            // { error }
                        });
                    //  typeCheck("Telegram")
                }}>
                    <View style={styles.bottomSheetStyle}>
                        <View style={[styles.iconStyles, { backgroundColor: '#27baba', paddingRight: responsiveWidth(1.2), paddingBottom: responsiveWidth(1) }]}>
                            <MaterialCommunityIcons color={'#ffff'} size={responsiveWidth(8)} name={'telegram'}></MaterialCommunityIcons>
                        </View>
                        <View>
                            <Text style={styles.textStyle}>Telegram</Text>
                        </View>

                    </View>
                </TouchableOpacity> */}

                <TouchableOpacity activeOpacity={0.7} onPress={() => { typeCheck("Email") }}>
                    <View style={styles.bottomSheetStyle}>
                        <View style={[styles.iconStyles, { backgroundColor: '#27baba' }]}>
                            <MaterialCommunityIcons color={'#ffff'} size={responsiveWidth(6)} name={'email'}></MaterialCommunityIcons>
                        </View>
                        <View>
                            <Text style={[styles.textStyle, { paddingLeft: responsiveWidth(2) }]}>Email</Text>
                        </View>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} onPress={() => { typeCheck("TWITTER") }}>
                    <View style={styles.bottomSheetStyle}>
                        <View style={[styles.iconStyles, { backgroundColor: '#27baba' }]}>
                            <MaterialCommunityIcons color={'#ffff'} size={responsiveWidth(8)} name={'twitter'}></MaterialCommunityIcons>
                        </View>
                        <View>
                            <Text style={[styles.textStyle, { paddingLeft: responsiveWidth(1.3) }]}>Twitter</Text>
                        </View>

                    </View>
                </TouchableOpacity>
            </View>
            {/* </RBSheet> */}
        </View>
    );
}
const styles = StyleSheet.create({
    flex: 1,
    bottomSheetStyle: {
        width: responsiveWidth(25),
        paddingVertical: responsiveWidth(3),
        paddingHorizontal: responsiveWidth(6),
    },
    iconStyles: {
        justifyContent: 'center', alignItems: 'center',
        borderRadius: responsiveWidth(5),
        width: responsiveWidth(10), height: responsiveWidth(10), backgroundColor: 'green'
    },
    textStyle: {
        textAlign: 'left', fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(1.1), paddingVertical: responsiveWidth(2)
    }
})


export default BottomSheetShare;