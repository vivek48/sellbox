
import Home from './Home'
import Settings from './Settings'
import SearchTab from './SearchTab'
import SellBoxDetails from './SellBoxDetails'
import TrendingNews from './TrendingNews'
import Dashboard from './Dashboard'
export { Home, Settings, SearchTab, SellBoxDetails, TrendingNews,Dashboard }