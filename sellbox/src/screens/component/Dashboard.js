import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, processColor, SafeAreaView, ScrollView } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { Chart, VerticalAxis, HorizontalAxis, Line } from 'react-native-responsive-linechart'
import { LineChart, PieChart } from 'react-native-charts-wrapper';

import { CommonFunctions, ServerConstant, Loader } from '../commoncomponent/index';
import AsyncStorage from '@react-native-community/async-storage';
export default class Dashboard extends Component {
    constructor() {
        super();
        this.state = {
            loading: false,
            token: '',
            legend: {
                enabled: true,
                textSize: 15,
                form: 'CIRCLE',
                horizontalAlignment: "RIGHT",
                verticalAlignment: "CENTER",
                orientation: "VERTICAL",
                wordWrapEnabled: true
            },
            data: '',
            // {
            //     dataSets: [
            //     ],
            // },
            highlights: [{ x: 2 }],
            description: {
                text: 'This is Sellbox data set  description',
                textSize: 15,
                // textColor: processColor('darkgray'),

            }
        };
    }



    destroyLoader = () => {
        setTimeout(() => this.setState({
            loading: false
        }), 2000);
    }
    showLoader = () => {
        this.setState({
            loading: true
        });

    }
    componentDidMount() {
        // this.props.navigation.addListener('focus', async () => {
        //     //   console.log("data set ----",this.state.data.dataSets);
        //     // this.state.data.dataSets.pop(),

        //     this.setState({ data: '' }, () => { this.getToken() })
        //     // this.getToken();
        // })
        this.getToken();
    }
    getToken = async () => {
        try {
            this.showLoader();
            const getToken = await AsyncStorage.getItem('UserToken');
            if (getToken !== null && getToken != undefined && getToken != '') {
                console.log(getToken);
                this.fetchDataByDate(getToken);
                this.setState({ token: getToken });
            } else {
                this.destroyLoader();
            }
        } catch (e) {
            this.destroyLoader();
        }
    }
    fetchDataByDate = async (Token) => {
        var today = new Date();
        var convertEndDate = today.toLocaleDateString();
        var yesterday = new Date(today.setDate(today.getDate() - 1));
        var convertStartDate = yesterday.toLocaleDateString();
        var urlencoded = new URLSearchParams();
        urlencoded.append('startDate', convertStartDate);
        urlencoded.append('endDate', convertEndDate);
        urlencoded.append('startFrom', 0);
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        myHeaders.append('Authorization', 'Bearer ' + Token);
        this.showLoader();
        await fetch(ServerConstant.serverPath + "/seller-box/advancedSearch", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                this.destroyLoader();
                if (result.status == 1) {
                    //  console.log("Dashboard---", result.result.esdata.length)

                    // if (result.result.esdata.length > 0) {
                    //     console.log(result.result.esdata.length, "-----");
                    //     var data = CommonFunctions.DashboardData(result.result.esdata);
                    //     console.log("data---", data);
                    //     if (data.length > 0) {
                    var graphData = {
                        dataSets: [{
                            values: [
                                { value: 64, label: 'Positive ' },
                                { value: 36, label: 'Neutral' },
                                // { value: data[2], label: 'Negative' },

                            ],
                            label: 'Sellbox data set',
                            config: {
                                colors: [processColor('#64B0C8'), processColor('#EAB839'),
                                    // processColor('#E24D42')
                                ],
                                valueTextSize: 20,
                                valueTextColor: processColor('#ffff'),
                                sliceSpace: 5,
                                selectionShift: 13,
                                // xValuePosition: "OUTSIDE_SLICE",
                                // yValuePosition: "OUTSIDE_SLICE",
                                valueFormatter: "#.#'%'",
                                valueLineColor: processColor('#fff'),
                                valueLinePart1Length: 0.5
                            }
                        }]
                    }

                    this.setState({ data: graphData });
                    //     }
                    // }

                } else if (result.status == 0) {
                } else {
                }
            }).catch((error) => {
                this.destroyLoader();
            });
    }
    handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({ ...this.state, selectedEntry: null })
        } else {
            this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
        }

        console.log(event.nativeEvent)
    }

    render() {
        console.log(this.state.data);
        return (
            <View>
                <Loader
                    loading={this.state.loading} />
                <SafeAreaView style={styles.safeAreaView}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{
                            color: Colors.AppBasicColor, fontSize: responsiveFontSize(2),
                            fontFamily: Fonts.Roboto.Roboto_Bold
                        }}>Sellbox Dashboard</Text>
                    </View>
                </SafeAreaView>
                <ScrollView>
                    <SafeAreaView style={styles.safeAreaViewDashboard}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }}>selected:</Text>
                            <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }}> {this.state.selectedEntry}</Text>
                        </View>
                        <View style={styles.container}>
                            {this.state.data !== null && this.state.data != '' && this.state.data != undefined &&
                                <PieChart
                                    style={styles.chart}
                                    logEnabled={true}
                                    // chartBackgroundColor={processColor('pink')}
                                    chartDescription={this.state.description}
                                    data={this.state.data}
                                    legend={this.state.legend}
                                    highlights={this.state.highlights}
                                    extraOffsets={{ left: 5, top: 5, right: 5, bottom: 5 }}
                                    entryLabelColor={processColor('#ffff')}
                                    entryLabelTextSize={20}
                                    entryLabelFontFamily={'HelveticaNeue-Medium'}
                                    drawEntryLabels={true}

                                    rotationEnabled={true}
                                    rotationAngle={45}
                                    usePercentValues={true}
                                    styledCenterText={{
                                        text: 'Pie center text!',
                                        color: processColor('pink'),
                                        fontFamily: 'HelveticaNeue-Medium', size: 20
                                    }}
                                    centerTextRadiusPercent={100}
                                    // holeRadius={40}
                                    holeRadius={0}
                                    holeColor={processColor('#f0f0f0')}
                                    transparentCircleRadius={45}
                                    transparentCircleColor={processColor('#f0f0f088')}
                                    maxAngle={350}
                                    onSelect={this.handleSelect.bind(this)}
                                    onChange={(event) => console.log(event.nativeEvent)}
                                />
                            }
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    flex: 1,
    safeAreaView: {
        paddingVertical: responsiveWidth(3),
        paddingHorizontal: responsiveWidth(2),
        backgroundColor: '#27baba'
    },
    safeAreaViewDashboard: {
        marginTop: responsiveWidth(2),
        height: responsiveHeight(82),
        marginHorizontal: responsiveWidth(2),
        backgroundColor: '#fefefe'
    },
    container: {
        flex: 1,
        // justifyContent:'center',
        // alignItems:'center'
        // backgroundColor: '#F5FCFF'
    },
    chart: {
        flex: 1
    },


})