import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Animated, ScrollView, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { setLogINUser, setDATA_INFO } from "../../stores/actions";
import { connect } from 'react-redux';
import { Images, Fonts, Colors } from '../../styles/Index'
import reducer from '../../stores/reducer';
import { CommonFunctions, ServerConstant, Loader, CustomRadioButton } from '../commoncomponent/index';
import AsyncStorage from '@react-native-community/async-storage';
import ResetPassword from './ResetPassword';
import ContactUs from './ContactUs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import BottomSheetShare from './BottomSheetShare'
class Settings extends Component {
    constructor() {
        super();
        this.state = {
            loading: false,
            token: '',
            settingsList: [
                {
                    label: 'Reset Password',
                    index: 1,
                },
                {
                    label: 'About Us',
                    index: 2,
                }
            ],
            selectedIndex: -1,

        }

    }

    destroyLoader = () => {
        setTimeout(() => this.setState({
            loading: false
        }), 100);
    }
    showLoader = () => {
        this.setState({
            loading: true
        });
    }

    logoutUser = async () => {
        try {
            const getToken = await AsyncStorage.removeItem('UserToken');
            // const getFCMToken = await AsyncStorage.removeItem('FCMToken');
            this.props.setLogINUser(false);
        } catch (e) {
            // error reading value
        }
    }


    // removeMemberDeviceData = async () => {
    //     const FCMToken = await AsyncStorage.getItem('FCMToken');
    //     var urlencoded = new URLSearchParams();
    //     urlencoded.append('deviceID', FCMToken);
    //     var myHeaders = new Headers();
    //     myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
    //     myHeaders.append('Authorization', 'Bearer '); //+ Token
    //     await fetch(ServerConstant.serverPath + "/user/removeDevice", {
    //         method: 'POST',
    //         headers: myHeaders,
    //         body: urlencoded.toString(),
    //         json: true,
    //     }).then((response) => response.json())
    //         .then((result) => {
    //             console.log(result);
    //             if (result.status == 1) {
    //                 this.logoutUser();
    //             } else if (result.status == 0) {
    //             } else {
    //             }

    //         })
    //         .catch((error) => {

    //         });
    // }
    closeButton = (value) => {
        this.setState({ selectedIndex: value })
    }

    render() {
        const { settingsList, selectedIndex } = this.state;
        return (
            <View style={{ backgroundColor: 'rgba(253, 253, 253, 1)' }} >
                <Loader
                    loading={this.state.loading} />
                <View style={styles.safeAreaView}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home") }}>
                        <View>
                            <Text style={{
                                fontSize: responsiveFontSize(2),
                                fontFamily: Fonts.Roboto.Roboto_Medium,
                                opacity: 0.7,
                                color: Colors.buttonColor
                            }}>Back</Text>
                        </View>
                    </TouchableOpacity>
                    <View>
                        <Text style={{
                            fontSize: responsiveFontSize(2),
                            fontFamily: Fonts.Roboto.Roboto_Medium,
                            opacity: 0.7,
                            color: 'rgba(4, 4, 4, 1)',
                        }}>Settings</Text>
                    </View>
                    <View />
                    {/* <View /> */}
                </View>
                <ScrollView>
                    <View>
                        {settingsList.map((item, index) => (
                            <View key={index}>
                                <View key={index} style={[styles.listHeaderSites, { borderBottomWidth: responsiveWidth(0.1) }]}>
                                    <TouchableOpacity activeOpacity={0.8} onPress={() => {
                                        if (this.state.selectedIndex == index) {
                                            this.setState({ selectedIndex: -1 })
                                        } else {
                                            this.setState({ selectedIndex: index })
                                        }
                                    }}>
                                        <View style={styles.listHeaderViewSites}>
                                            <View style={{ width: responsiveWidth(88.3) }}>
                                                <Text style={styles.listHeaderTextSites}>{item.label}</Text>
                                            </View>
                                            <View>
                                                <MaterialIcons size={responsiveWidth(7)} name={this.state.selectedIndex == index ? 'keyboard-arrow-down' : 'keyboard-arrow-right'}></MaterialIcons>
                                            </View>
                                        </View>

                                    </TouchableOpacity>
                                </View>
                                {this.state.selectedIndex == index && index == 0 &&
                                    <View style={{ paddingBottom: responsiveWidth(3), borderBottomWidth: responsiveWidth(0.1), }}>
                                        <View style={{ marginHorizontal: responsiveWidth(3.7), }}>
                                            <ResetPassword closeFunction={this.closeButton} />
                                        </View>
                                    </View>
                                }
                                {this.state.selectedIndex == index && index == 1 &&
                                    <View style={{ paddingBottom: responsiveWidth(3), borderBottomWidth: responsiveWidth(0.1), }}>
                                        <View style={{ marginLeft: responsiveWidth(10), marginRight: responsiveWidth(3) }}>
                                            <ContactUs closeFunction={this.closeButton} />
                                        </View>
                                    </View>
                                }


                            </View>
                        ))}
                        <View style={{
                            // borderTopWidth: responsiveWidth(0.1),
                            height: responsiveHeight(10)
                        }}>
                            <TouchableOpacity onPress={() => { this.logoutUser(); }}>
                                <View style={{
                                    borderColor: 'rgba(220, 220, 222, 1)'
                                    , borderWidth: responsiveWidth(0.2), borderRadius: responsiveWidth(2),
                                    justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6),
                                    marginVertical: responsiveWidth(4),
                                    marginHorizontal: responsiveWidth(3), backgroundColor: Colors.buttonColor //'rgba(247, 247, 247, 1)'
                                }}>
                                    {/* color: 'rgba(150, 149, 151, 1)' */}
                                    <Text style={{ color: Colors.NewsColor, fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }}>Log Out</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        {/* <BottomSheetShare></BottomSheetShare> */}
                    </View>
                </ScrollView>
            </View >
        );
    }
}
const mapStateToProps = state => {
    return {
        loginuser: state.loginuser,
        dataInfo: state.dataInfo,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLogINUser: data => {
            dispatch(setLogINUser(data));
        },
        setDATA_INFO: data => {
            dispatch(setDATA_INFO(data));
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Settings);
const styles = StyleSheet.create({
    flex: 1,
    safeAreaView: {
        paddingLeft: responsiveWidth(3),
        paddingRight: responsiveWidth(12),
        height: responsiveHeight(7),
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#27baba',
        flexDirection: 'row'
    },
    notification: {
        width: responsiveWidth(100),
        paddingTop: responsiveWidth(6),
        paddingHorizontal: responsiveWidth(7),
        paddingBottom: responsiveWidth(2),
        borderTopWidth: responsiveWidth(0.1),
        borderBottomWidth: responsiveWidth(0.1),
        backgroundColor: 'rgba(246, 246, 246, 1)'
    },
    commonHeader: {
        fontSize: responsiveFontSize(1.7),
        fontFamily: Fonts.Roboto.Roboto_Medium, color: 'rgba(141, 141, 141, 1)'
    },
    listHeaderSites: {
        alignContent: 'center', justifyContent: 'center',
        paddingHorizontal: responsiveWidth(3),
        height: responsiveHeight(8),
    },
    listHeaderViewSites: {
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
        paddingVertical: responsiveWidth(1),
        flexDirection: 'row'
    },
    listHeaderTextSites: {
        opacity: 0.7,
        fontSize: responsiveFontSize(1.9),
        fontFamily: Fonts.Roboto.Roboto_Medium,
    },
})

