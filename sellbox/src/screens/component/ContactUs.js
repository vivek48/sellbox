import React, { Component, useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Animated, ScrollView, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { setLogINUser, setDATA_INFO } from "../../stores/actions";
import { connect } from 'react-redux';
import { Images, Fonts, Colors } from '../../styles/Index'
import reducer from '../../stores/reducer';
import { CommonFunctions, ServerConstant, Loader, CustomRadioButton } from '../commoncomponent/index';
import AsyncStorage from '@react-native-community/async-storage';

const ContactUs = ({ closeFunction }, props) => {
    return (
        <View>
            <View style={styles.commonViewPadding}>
                <Text style={[styles.labelView, { paddingTop: responsiveWidth(2) }]}>
                    SAMV@D is a Digital Analytics Platform that helps to understand views expressed in Print, Television, Digital and Social Media Platforms vis-à-vis government policies and strategies.
                    </Text>
                <Text style={[styles.labelView, { paddingTop: responsiveWidth(3) }]}>
                    It enables authorities in gathering indirect feedback from the public, thereby facilitating key decision making. Its advanced search mechanism dispenses relevant information sorted on the basis of various filters such as sentiment, location, language, source etc.
                    </Text>
            </View>
            <View style={{ flexDirection: 'row', paddingTop: responsiveWidth(3), justifyContent: 'space-around' }}>
                <View />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    flex: 1,
    buttonStyles: {
        justifyContent: 'center',
        height: responsiveWidth(9), alignItems: 'center', borderRadius: responsiveWidth(2),
        width: responsiveWidth(36), backgroundColor: 'yellow',
    },
    commonViewPadding: {
        paddingTop: responsiveWidth(2),
        paddingRight: responsiveWidth(3)
    },
    labelView: {
        fontSize: responsiveFontSize(1.7), color: '#0e2c6e', fontFamily: Fonts.Roboto.Roboto_Black
    },

})


export default ContactUs;