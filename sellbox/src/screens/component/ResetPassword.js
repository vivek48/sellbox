import React, { Component, useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Animated, ScrollView, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { setLogINUser, setDATA_INFO } from "../../stores/actions";
import { connect } from 'react-redux';
import { Images, Fonts, Colors } from '../../styles/Index'
import reducer from '../../stores/reducer';
import { CommonFunctions, ServerConstant, Loader, CustomRadioButton } from '../commoncomponent/index';
import AsyncStorage from '@react-native-community/async-storage';

import Toast from 'react-native-simple-toast';

const ResetPassword = ({ closeFunction }, props) => {

    const [emailID, setEmailID] = useState('');
    const [oldPassword, setOldPassword] = useState('');
    const [oldPasswordError, setOldPasswordError] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [newPasswordError, setNewPasswordError] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [confirmNewPasswordError, setConfirmNewPasswordError] = useState('');



    useEffect(() => {
        getStoreUserData();
    }, [])
    const getStoreUserData = async () => {
        try {
            const userData = JSON.parse(await AsyncStorage.getItem('UserInformation'));
            // console.log("userData ", userData.UserEmail);
            setEmailID(userData.UserEmail)
        } catch (e) {
        }
    }


    const userOldPass = (value) => {
        setOldPassword(value);
        setOldPasswordError(false);
    }
    const userNewPass = (value) => {
        setNewPassword(value);
        setNewPasswordError(false);
    }

    const userConfirmNewPass = (value) => {
        setConfirmNewPassword(value);
        setConfirmNewPasswordError(false);
    }



    const resetUserPassword = async () => {
        var urlencoded = {};
        var requestData = {};
        if (oldPassword.trim() == '' || oldPassword == undefined || oldPassword == null || oldPasswordError) {
            setOldPasswordError(true)
            return true;
        } else {
            urlencoded.EmailId = emailID.trim();
            urlencoded.CurrentPass = oldPassword.trim();
            setOldPasswordError(false)
        }
        if (newPassword.trim() == '' || newPassword == undefined || newPassword == null) {
            setNewPasswordError(true)
            return true;
        } else {
            urlencoded.NewPassword = newPassword.trim();
            setNewPasswordError(false)
        }
        if (confirmNewPassword.trim() == '' || confirmNewPassword == undefined || confirmNewPassword == null) {
            setConfirmNewPasswordError(true)
            return true;
        } else {
            urlencoded.Confirmpass = confirmNewPassword.trim();
            urlencoded.platform = "APP";
            setConfirmNewPasswordError(false)
        }
        requestData.requestData = urlencoded;
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json')
        await fetch(ServerConstant.serverPath + '/ResetPassword', {
            method: 'POST',
            // "/authenticateUser"
            headers: myHeaders,
            body: JSON.stringify(requestData),//.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                console.log(result);

                if (result.status == 1) {
                    Toast.show(result.description, Toast.LONG,);

                } else if (result.status == 0) {
                    Toast.show(result.description, Toast.LONG,);

                } else {

                }
            }).catch((error) => {
                Toast.show(error, Toast.LONG,);

            });
    }


    return (
        <View>
            <View style={styles.commonViewPadding}>
                <Text style={styles.labelView}>
                    User Mail ID</Text>
            </View>
            <View style={styles.commonViewPadding}>
                <TextInput style={[styles.textInputStyles, { backgroundColor: 'lightgrey' }]}
                    name='Email'
                    editable={false}
                    value={emailID}
                    onChangeText={(Email) => { }}></TextInput>
            </View>
            <View style={styles.commonViewPadding}>
                <Text style={styles.labelView}>
                    Current Password*</Text>
            </View>
            <View style={styles.commonViewPadding}>
                <TextInput style={styles.textInputStyles}
                    name='Password'
                    secureTextEntry={true}
                    value={oldPassword}
                    onChangeText={(oldPassword) => { userOldPass(oldPassword) }}></TextInput>
            </View>
            {oldPasswordError ?
                <View style={{ paddingTop: responsiveWidth(1), }}>
                    <Text style={styles.errorColor}>Current password required.</Text>
                </View> : null
            }
            <View style={styles.commonViewPadding}>
                <Text style={styles.labelView}>
                    New Password*</Text>
            </View>
            <View style={styles.commonViewPadding}>
                <TextInput style={styles.textInputStyles}
                    name='Password'
                    secureTextEntry={true}
                    value={newPassword}
                    onChangeText={(newPassword) => { userNewPass(newPassword) }}></TextInput>
            </View>
            {newPasswordError ?
                <View style={{ paddingTop: responsiveWidth(1), }}>
                    <Text style={styles.errorColor}>New password required.</Text>
                </View> : null
            }
            <View style={styles.commonViewPadding}>
                <Text style={styles.labelView}>
                    Confirm New Password*</Text>
            </View>
            <View style={styles.commonViewPadding}>
                <TextInput style={styles.textInputStyles}
                    name='Password'
                    secureTextEntry={true}
                    value={confirmNewPassword}
                    onChangeText={(newConfirmPassword) => { userConfirmNewPass(newConfirmPassword) }}></TextInput>
            </View>

            {confirmNewPasswordError ?
                <View style={{ paddingTop: responsiveWidth(1), }}>
                    <Text style={styles.errorColor}>Confirm and New password must be match.</Text>
                </View> : null
            }
            <View style={{ flexDirection: 'row', paddingTop: responsiveWidth(3), justifyContent: 'space-around' }}>
                <View />
                <TouchableOpacity activeOpacity={0.7} onPress={() => { resetUserPassword() }}>
                    <View style={styles.buttonStyles}>
                        <Text style={{ fontSize: responsiveFontSize(2), }}>Reset Password</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    flex: 1,
    buttonStyles: {
        justifyContent: 'center',
        // marginHorizontal:responsiveWidth(3),
        height: responsiveWidth(10),
        alignItems: 'center',
        borderRadius: responsiveWidth(2),
        width: responsiveWidth(91.7),
        backgroundColor: '#ffc107',
    },
    commonViewPadding: {
        paddingTop: responsiveWidth(1.2),
    },
    textInputStyles: {
        backgroundColor: '#fff',
        borderColor: 'grey',
        borderWidth: responsiveWidth(0.2),
        height: responsiveHeight(5),
        paddingHorizontal: responsiveWidth(2),
        borderRadius: responsiveWidth(3)
    },
    labelView: {
        // color:'red',
        fontSize: responsiveFontSize(1.6), color: '#0e2c6e', fontFamily: Fonts.Roboto.Roboto_Black
    },
    errorColor: {
        color: 'red', fontSize: responsiveFontSize(1.5)
    }

})


export default ResetPassword;