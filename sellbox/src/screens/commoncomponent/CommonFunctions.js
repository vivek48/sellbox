import RNFetchBlob from 'rn-fetch-blob'

import { View, Text, Image, TextInput, StyleSheet, processColor, SafeAreaView, ScrollView } from 'react-native';
export const CommonFunctions = {
    //  userToken: false,
    gray: '#797777',
    ConvertDate: (Stringdate) => {
        // console.log(Stringdate);
        var date = new Date(Stringdate);
        var LocalDate = date.toLocaleString('en-US', { timeZone: 'Asia/Kolkata' })
        const diffTime = Math.abs(new Date() - date)
        const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24))
        //Get Days
        if (diffDays == 1) {
            return 'a day'// ago'
        }
        else if (diffDays > 0 && diffDays < 5) {
            return diffDays + ' days'// ago'
        }
        else if (diffDays == 0) {
            //Get Hours
            const diffHours = Math.floor(diffTime / (1000 * 60 * 60))
            if (diffHours == 1) {
                return 'an hour'// ago'
            }
            else if (diffHours > 0) {
                return diffHours + ' hours'// ago'
            }
            else {
                //Get Minutes
                const diffMinutes = Math.floor(diffTime / (1000 * 60))
                if (diffMinutes < 2) {
                    return 'Just'// now'
                }
                else {
                    return diffMinutes + ' minutes'// ago'
                }
            }
        }
        var date = LocalDate.split(', ')[0]
        var time = LocalDate.split(', ')[1].split(' ')[0]
        var ampm = LocalDate.split(', ')[1].split(' ')[1]
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var strTime = months[parseInt(date.split('/')[0]) - 1] + ' ' + date.split('/')[1] + ' ' + date.split('/')[2]
            + ', ' + time.split(':')[0] + ':' + time.split(':')[1] + ' ' + ampm
        /*var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = months[date.getMonth()] + ' ' + date.getDate() + ' ' + date.getFullYear() + ', ' + hours + ':' + minutes + ' ' + ampm;*/
        return strTime
    },

    convertReachedPeople: (ReachedUser) => {
        var convertNumber = Number(ReachedUser);
        var totolReachedUser = '';
        if (convertNumber >= 1000 && convertNumber <= 1000000) {
            totolReachedUser = convertNumber / 1000 + ' K';

        } else if (convertNumber >= 1000000 && convertNumber <= 10000000) {
            totolReachedUser = convertNumber / 1000000 + ' M';
        } else if (convertNumber > 10000000 && convertNumber <= 1000000000) {
            totolReachedUser = convertNumber / 10000000 + ' Cr';

        } else {
            totolReachedUser = convertNumber;
        }
        return totolReachedUser;
    },
    check_AM_PM: () => {
        var splitHourse = new Date().toLocaleTimeString();
        //var splitHourse = getDateAndTime.split(' ');
        //var splitHourseMinute = splitHourse.split(':');
        var getValue = '';
        if (splitHourse != '23:59:59 PM' && splitHourse <= '11:59:59 PM') {
            getValue = "Good Morning";
        } else if (splitHourse >= '12:00:00 PM' && splitHourse <= '17:00:00 PM') {
            getValue = "Good Afternoon";

        } else if (splitHourse >= '17:00:00 PM' && splitHourse <= '23:59:59 PM') {
            getValue = "Good Evening";

        }
        return getValue;
    },

    imageCovertArray: (image) => {
        var articleImages = image.toString()
        //  let data=result.result[0]._source.contenturl;
        return articleImages = JSON.parse(articleImages.replace(/'/g, '"'))
    },

    changeDateFormat: (date) => {
        // console.log('----', date);
        var splitDate = date.split('/');
        var yearSplit = (new Date(date)).toDateString().split('-')[0].split(' ');
        switch (splitDate[0]) {
            case '01':
                return splitDate[1] + '-Jan-' + yearSplit[3];
                break;
            case '02':
                return splitDate[1] + '-Feb-' + yearSplit[3];
                break;
            case '03':
                return splitDate[1] + '-Mar-' + yearSplit[3];
                break;
            case '04':
                return splitDate[1] + '-Apr-' + yearSplit[3];
                break;
            case '05':
                return splitDate[1] + '-May' + yearSplit[3];
                break;
            case '06':
                return splitDate[1] + '-Jun-' + yearSplit[3];
                break;
            case '07':
                return splitDate[1] + '-Jul-' + yearSplit[3];
                break;
            case '08':
                return splitDate[1] + '-Aug-' + yearSplit[3];
                break;
            case '09':
                return splitDate[1] + '-Sep-' + yearSplit[3];
                break;
            case '10':
                return splitDate[1] + '-Oct-' + yearSplit[3];
                break;
            case '11':
                return splitDate[1] + '-Nov-' + yearSplit[3];
                break;
            case '12':
                return splitDate[1] + '-Dec-' + yearSplit[3];
                break;

        }
    },






    changeNewsDetailDateFormat: (date) => {
        // 2021-01-04
        console.log('----', new Date(date).toLocaleDateString().split('-'));
        var splitDate = new Date(date).toLocaleDateString().split('-')[0].split('/')
        var yearSplit = (new Date(date)).toDateString().split('-')[0].split(' ');
        switch (splitDate[0]) {
            case '01':
                return splitDate[1] + '-Jan-' + yearSplit[3];
                break;
            case '02':
                return splitDate[1] + '-Feb-' + yearSplit[3];
                break;
            case '03':
                return splitDate[1] + '-Mar-' + yearSplit[3];
                break;
            case '04':
                return splitDate[1] + '-Apr-' + yearSplit[3];
                break;
            case '05':
                return splitDate[1] + '-May' + yearSplit[3];
                break;
            case '06':
                return splitDate[1] + '-Jun-' + yearSplit[3];
                break;
            case '07':
                return splitDate[1] + '-Jul-' + yearSplit[3];
                break;
            case '08':
                return splitDate[1] + '-Aug-' + yearSplit[3];
                break;
            case '09':
                return splitDate[1] + '-Sep-' + yearSplit[3];
                break;
            case '10':
                return splitDate[1] + '-Oct-' + yearSplit[3];
                break;
            case '11':
                return splitDate[1] + '-Nov-' + yearSplit[3];
                break;
            case '12':
                return splitDate[1] + '-Dec-' + yearSplit[3];
                break;

        }
    },


    getBase64: async (url) => {
        var baseUrl = null;
        await RNFetchBlob.fetch('GET', url, {
        })
            .then((res) => {
                let status = res.info().status;
                if (status == 200) {
                    let base64Str = res.base64()
                    baseUrl = base64Str;
                    return baseUrl;
                } else {
                    return null;
                }
            }).catch((errorMessage, statusCode) => {
                return null
            })
        return baseUrl;
    },


    DashboardData: (products) => {

        var neutralData = 0;
        var positiveData = 0;
        // var negativeData = 0;
        var data = [];
        for (var i = 0; i < products.length; i++) {
            // console.log(" Check ------", products[i]._source.sellBox);
            if (products[i]._source.sellBox) {
                positiveData = positiveData + 1;
            } else {
                negativeData = negativeData + 1;
            }
            if (i == products.length - 1) {
                data.push(positiveData);
                data.push(neutralData);
            }
        }
        console.log("News Data Length ", products.length, " Positive Data", positiveData, "Neutral Data", neutralData, data);
        return data;
    }
}
export default CommonFunctions;