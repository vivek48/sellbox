import React from 'react'
import { Animated, Dimensions, Image, } from 'react-native'
import PhotoView from 'react-native-photo-view-ex';
import { responsiveWidth, responsiveHeight, responsiveFontSize, responsiveScreenWidth, responsiveScreenHeight } from 'react-native-responsive-dimensions';
const screen = Dimensions.get('window')
const ZoomImage = ({ imageUri }) => {
  console.log(imageUri);
  return (
    <PhotoView
      source={{ uri: imageUri }}
      minimumZoomScale={1.0}
      maximumZoomScale={3}
      resizeMode="center"
      onLoad={() => console.log("Image loaded!")}
      style={{ marginHorizontal: responsiveWidth(4), height: responsiveScreenHeight(50) }}
    />
  )
}

export default ZoomImage;