import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, Animated } from 'react-native';

import SplashScreen from 'react-native-splash-screen'

export default class Demo extends Component {
    // constructor() {
    //     super();
    //     this.
        state = {
            // First Example
            // animation: new Animated.Value(1)

            // Second Example
            // animation: new Animated.Value(0)

            // Third Example
            // animation: new Animated.Value(1)

            // 4th Example not supported

            //animation: new Animated.Value(150)

            animation: new Animated.Value(0)


        }
    // }

    componentDidMount() {
        SplashScreen.hide();
    }
    // First Example
    // startAnimation = () => {
    //     Animated.timing((this.state.animation), {
    //         toValue: 0,
    //         duration: 4000,
    //         useNativeDriver: true

    //     }).start(() => {
    //         Animated.timing(this.state.animation, {
    //             toValue: 1,
    //             duration: 500,
    //             useNativeDriver: true
    //         }).start();
    //     });
    // }
    // Second Example
    // startAnimation = () => {
    //     Animated.timing(this.state.animation, {
    //         toValue: 300,
    //         duration: 4500,
    //         useNativeDriver: true
    //     }).start(() => {
    //         Animated.timing(this.state.animation, {
    //             toValue: -400,
    //             duration: 4500,
    //             useNativeDriver: true
    //         }).start()
    //     })
    // }

    // Third Example
    // startAnimation = () => {
    //     Animated.timing(this.state.animation, {
    //         toValue: 2,
    //         duration: 5000,
    //         useNativeDriver: true
    //     }).start(() => {
    //         Animated.timing(this.state.animation, {
    //             toValue: -1,
    //             duration: 5000,
    //             useNativeDriver: true
    //         }).start()

    //     })
    // }



    // 4th Example not supported
    // startAnimation = () => {
    //     Animated.timing(this.state.animation, {
    //         toValue: 200,
    //         duration: 500,
    //         useNativeDriver: true
    //     }).start()
    // }


    // 5th Example not working

    // startAnimation = () => {
    //     Animated.timing(this.state.animation, {
    //         toValue: 10,
    //         duration: 300,
    //         useNativeDriver: true
    //     }).start()

    // }
    render() {

        // 5th Example not working

        // const animationStyles = {
        //     top: this.state.animation,
        //     left: this.state.animation

        // }

        // 4th Example not supported
        // const animationStyles = {
        //     width: this.state.animation,
        //     height: this.state.animation,
        // }

        // Third Example

        // const animationStyles = {
        //     transform: [
        //         {
        //             scale: this.state.animation
        //         }
        //     ]
        // }


        // Second Example
        // const animationStyles = {
        //     transform: [
        //         {
        //             translateY: this.state.animation
        //         }
        //     ]
        // }


        // First Example
        // const animationStyles = {
        //     opacity: this.state.animation
        // }

        const boxInterpolation = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: ['rgb(255,99,71)', 'rgb(99,71,255)']

        })
        const boxAnimatedStyle = {
            backgroundColor: boxInterpolation
        }

        const colorInterpolation = this.state.animation({
            inputRange: [0, 1],
            outputRange: ['rgb(99,71,255)', 'rgb(255,99,71)']
        })
        const boxTextAnimatedValue={
            color:colorInterpolation
        }

        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.startAnimation}>
                    <Animated.View style={[styles.box, animationStyles]}>
                        <Text>Hello Baby Gand Marao</Text>

                    </Animated.View>

                </TouchableWithoutFeedback>

            </View >
        );
    }
}

const styles = StyleSheet.create({
    flex: 1,
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    box: {
        height: 150,
        position: 'absolute',
        top: 0,
        left: 0,
        width: 150,
        backgroundColor: 'tomato'
    }



})