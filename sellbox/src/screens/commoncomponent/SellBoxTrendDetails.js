import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, SafeAreaView, ScrollView, Share, TouchableOpacity } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize, } from 'react-native-responsive-dimensions';
import { Fonts, Colors, Images } from '../../styles/Index'
import { WebView } from 'react-native-webview';
const SellBoxTrendDetails = (props) => {
    return (
        <View style={{ height: responsiveHeight(90) }}>
            <SafeAreaView style={styles.safeAreaView}>
                <TouchableOpacity onPress={() => { props.navigation.navigate('Trending') }}>
                    <View>
                        <Text style={{

                            fontSize: responsiveFontSize(2),
                            fontFamily: Fonts.Roboto.Roboto_Medium,
                            opacity: 0.7,
                            color: Colors.buttonColor
                        }}>Back</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ paddingRight: responsiveWidth(4) }}>
                    <Text style={{
                        color: Colors.AppBasicColor, fontFamily: Fonts.Roboto.Roboto_Medium, fontSize: responsiveFontSize(2)
                    }}>SellBox Trending Detail</Text>
                </View>
                <TouchableOpacity onPress={() => { props.navigation.navigate('Settings') }} >
                    <View>
                        <Image source={Images.menu} tintColor={Colors.buttonColor} style={{ width: responsiveWidth(5), height: responsiveWidth(5) }}></Image>
                    </View>
                </TouchableOpacity>

            </SafeAreaView>
            <View style={{ width: responsiveWidth(100), height: responsiveHeight(90) }}>
                <ScrollView>
                    {props.route.params.Uri !== undefined &&
                        <View style={{ width: responsiveWidth(100), height: responsiveHeight(90) }}>
                            <WebView source={{ uri: props.route.params.Uri }} />
                        </View>
                    }
                </ScrollView>
            </View>
        </View >
    );

}
export default SellBoxTrendDetails;

const styles = StyleSheet.create({
    flex: 1,
    safeAreaView: {


        paddingHorizontal: responsiveWidth(3),
        height: responsiveHeight(7),
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#27baba',
        flexDirection: 'row'



        // width: responsiveWidth(100),
        // paddingTop: responsiveWidth(2),
        // paddingHorizontal: responsiveWidth(4),
        // paddingBottom: responsiveWidth(1.5),
        // backgroundColor: '#27baba'
    },
})