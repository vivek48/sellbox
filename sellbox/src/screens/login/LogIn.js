import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { CommonFunctions, ServerConstant, Loader } from '../commoncomponent/index';
import { setLogINUser, setCheckUserLogin } from "../../stores/actions";
import { connect } from 'react-redux';
import { Images, Fonts, Colors } from '../../styles/Index'
import reducer from '../../stores/reducer';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';
// import ConfigFirebase from '../commoncomponent/ConfigFirebase';
// import firebase from '@react-native-firebase/app';
// import iid from '@react-native-firebase/iid';
// import messaging from '@react-native-firebase/messaging';
var loading = false;
class LogIn extends Component {
    _isMounted = false;
    constructor() {
        super();
        this.state = {
            EmailID: '',
            EmailIDError: false,
            Password: '',
            PasswordError: false,
            //  loading: false,
            LoginUserNotExist: false,
        }
    }
    storeData = async (value) => {
        try {
            await AsyncStorage.setItem('UserToken', value);
            this.props.setLogINUser(true);
            this.props.setCheckUserLogin(true);
        } catch (e) {
            // saving error
        }
    }
    destroyLoader = () => {
        setTimeout(() =>
            loading = false
            , 2000);
    }
    showLoader = () => {
        loading = true
    }


    componentDidMount() {
        this._isMounted = true;
        // do stuff while splash screen is shown
        // After having done stuff (such as async tasks) hide the splash screen
        SplashScreen.hide();
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    checkLoginUserData = (type, data) => {
        if (type == 'EmailID') {
            if (data.trim() == '' || data == undefined || data == null) {
                this.setState({ EmailIDError: true, EmailID: data });
            } else {
                this.setState({ EmailIDError: false, EmailID: data });
            }
        }
        if (type == 'Password') {
            if (data.trim() == '' || data == undefined || data == null) {
                this.setState({ PasswordError: true, Password: data });
            } else {
                this.setState({ PasswordError: false, Password: data });
            }
        }
    }
    loginUser = async () => {
        var urlencoded = new URLSearchParams();
        if (this.state.EmailID.trim() == '' || this.state.EmailID == undefined || this.state.EmailID == null) {
            this.setState({ EmailIDError: true });
            return true;
        } else {
            urlencoded.append('txtUserName', this.state.EmailID.trim());
            this.setState({ EmailIDError: false });
        }

        if (this.state.Password.trim() == '' || this.state.Password == undefined || this.state.Password == null || this.state.PasswordError) {
            this.setState({ PasswordError: true });
            return true;
        } else {
            urlencoded.append('txtPassword', this.state.Password.trim());
            urlencoded.append('platform', 'APP');
            this.setState({ PasswordError: false });
        }
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        this.showLoader();
        await fetch(ServerConstant.serverPath + "/authenticateUser", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                // if (this._isMounted) {

                console.log(result);
                if (result.status == 1) {
                    if (this._isMounted) {
                        // Remove Firebase token store
                        this.storeData(result.token);
                        //this.checkFireBaseInitialize(result.token);

                        this.storeUserData(result.result)
                        this.destroyLoader();
                    }
                } else if (result.status == 0) {
                    loading = false
                    // this.destroyLoader();
                } else {
                    loading = false
                    this.setState({ LoginUserNotExist: true });
                    // this.destroyLoader();
                }
                // }
            }).catch((error) => {
                alert(error);
                this.destroyLoader();
                //  this.destroyLoader();
            });
    }



    // checkFireBaseInitialize = async (UserToken) => {
    //     if (firebase.apps.length === 0) {
    //         firebase.initializeApp(ConfigFirebase, UserToken)
    //         const id = await iid().get();
    //         const FCMToken = await iid().getToken('599482376734', '*');
    //         this.saveMemberDeviceData(FCMToken, UserToken);
    //     } else {
    //         const id = await iid().get();
    //         const FCMToken = await iid().getToken('599482376734', '*');
    //         this.saveMemberDeviceData(FCMToken, UserToken);
    //     }
    // }
    saveMemberDeviceData = async (FCMToken, UserToken) => {
        var urlencoded = new URLSearchParams();
        urlencoded.append('deviceID', FCMToken);
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        myHeaders.append('Authorization', 'Bearer '); //+ Token
        await fetch(ServerConstant.serverPath + "/user/addNewDevice", {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded.toString(),
            json: true,
        }).then((response) => response.json())
            .then((result) => {
                console.log(result);
                if (result.status == 1) {
                    this.storeFCMTokenData(FCMToken);
                    this.storeData(UserToken);
                    loading = false
                } else if (result.status == 0) {
                    // loading = false
                    this.destroyLoader()
                } else {
                    this.destroyLoader()
                    // loading = false
                }
            })
            .catch((error) => {
                loading = false
            });
    }

    storeFCMTokenData = async (FCMToken) => {
        console.log("TOKEN FCM", FCMToken);
        try {
            await AsyncStorage.setItem('FCMToken', FCMToken);
        } catch (e) {
        }
    }


    storeUserData = async (userData) => {
        console.log("userData ", userData);
        try {
            await AsyncStorage.setItem('UserInformation', JSON.stringify(userData));
        } catch (e) {
        }
    }
    render() {
        const { EmailIDError, LoginUserNotExist, EmailID, Password, PasswordError } = this.state;
        console.log(loading);
        return (
            <ScrollView >
                <ImageBackground source={Images.login_bg} style={{ width: responsiveWidth(100), height: responsiveHeight(100) }}>
                    <SafeAreaView style={styles.safeAreaView}>
                        <View style={{}}>
                            <Text style={styles.headerView}>Please sign in to your account</Text>
                        </View>
                        <Loader
                            loading={loading} />

                        {LoginUserNotExist ?
                            <View style={{ paddingTop: responsiveWidth(1), paddingLeft: responsiveWidth(4) }}>
                                <Text style={styles.errorColor}>Invalid login attempt.</Text>
                            </View> : null
                        }


                        <View style={styles.commonViewPadding}>
                            <Text style={styles.labelView}>Username/Email</Text>
                        </View>
                        <View style={styles.commonViewPadding}>
                            <TextInput style={styles.textInputStyles}
                                name='Email'
                                value={this.state.EmailID}
                                onChangeText={(Email) => { this.checkLoginUserData('EmailID', Email) }}></TextInput>
                        </View>
                        {EmailIDError ?
                            <View style={{ paddingTop: responsiveWidth(1) }}>
                                <Text style={styles.errorColor}>The Username/Email field is required.</Text>
                            </View> : null
                        }
                        <View style={styles.commonViewPadding}>
                            <Text style={styles.labelView}>
                                Password</Text>
                        </View>
                        <View style={styles.commonViewPadding}>
                            <TextInput style={styles.textInputStyles}
                                name='Password'
                                secureTextEntry={true}
                                value={Password}
                                onChangeText={(Password) => { this.checkLoginUserData('Password', Password) }}></TextInput>
                        </View>
                        {PasswordError ?
                            <View style={{ paddingTop: responsiveWidth(1), }}>
                                <Text style={styles.errorColor}>The Password field is required.</Text>
                            </View> : null
                        }
                        <View style={styles.commonViewPadding}>
                            <TouchableOpacity onPress={() => { this.loginUser() }}>

                                <View style={{
                                    justifyContent: 'center', alignItems: 'center',
                                    height: responsiveHeight(6), backgroundColor: '#0e2c6e',
                                    borderRadius: responsiveWidth(3)

                                }}>
                                    <Text style={{ fontSize: responsiveFontSize(2), color: 'white', fontFamily: Fonts.Roboto.Roboto_Black }}>Log in</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                </ImageBackground>
            </ScrollView >
        );
    }
}
// const mapStateToProps = state => {
//     return {
//         loginuser: state.loginuser
//     };
// };
const mapDispatchToProps = dispatch => {
    return {
        setLogINUser: data => {
            dispatch(setLogINUser(data));
        },
        setCheckUserLogin: data => {
            dispatch(setCheckUserLogin(data));
        },
    };
};
export default connect(null, mapDispatchToProps)(LogIn);

const styles = StyleSheet.create({
    flex: 1,
    safeAreaView: {
        justifyContent: 'center', width: responsiveWidth(100),
        height: responsiveHeight(100),
        paddingHorizontal: responsiveWidth(10),
        // backgroundColor: '#17a2b8'//'#6c757d'// '#27baba'
    },
    commonViewPadding: {
        paddingTop: responsiveWidth(2),
    },
    textInputStyles: {
        backgroundColor: '#fff',
        borderColor: 'grey',
        borderWidth: responsiveWidth(0.2),
        height: responsiveHeight(6),
        paddingHorizontal: responsiveWidth(2),
        borderRadius: responsiveWidth(3)
    },
    headerView: {
        fontSize: responsiveFontSize(3), color: '#0e2c6e', fontFamily: Fonts.Roboto.Roboto_Bold
    },
    labelView: {
        fontSize: responsiveFontSize(2), color: '#0e2c6e', fontFamily: Fonts.Roboto.Roboto_Black
    },
    errorColor: {
        color: 'red', fontSize: responsiveFontSize(1.5)
    }

})
