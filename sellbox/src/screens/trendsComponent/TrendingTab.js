import React, { useContext, useState, useEffect } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Images, Fonts, Colors } from '../../styles/Index'
import LatestNews from './LatestNews'
import SellBoxTrend from './SellBoxTrend'
import { MyContext } from '../../../App'
const initialLayout = { width: Dimensions.get('window').width };
import { StyleSheet, Dimensions, BackHandler } from 'react-native';
export default function TrendingTab(props) {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'SellBoxTrend', title: 'SellBox Trends', navigation: props },
    { key: 'LatestNews', title: 'Latest News', navigation: props },
  ]);

  // useEffect(()=>{
  //   props.navigation.navigate('Trending');
  // })


  const renderScene = SceneMap({
    SellBoxTrend: SellBoxTrend,
    LatestNews: LatestNews,
  });
  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      {...props}
      initialLayout={initialLayout}
      renderTabBar={(props) => {
        return (
          <TabBar
            {...props}
            renderIndicator={() => null}
            onTabPress={({ route }) => {
              console.log(route);
            }}
            //  renderIcon={renderIcon}
            //  renderLabel={() => null}
            activeColor="#ffff"
            inactiveColor="#c2c3c8"
            indicatorStyle={{ backgroundColor: '#000' }}
            style={{ backgroundColor: '#27baba', }}
          />
        )
      }}

    />
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});