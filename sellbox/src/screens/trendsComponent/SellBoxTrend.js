import React, { useState, useEffect, useContext } from 'react';
import { Text, Linking, FlatList, TouchableOpacity, View, StyleSheet, BackHandler } from 'react-native';
import { CommonFunctions, Loader, ServerConstant } from '../commoncomponent/index'
import { Images, Fonts, Colors } from '../../styles/Index'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
const SellBoxTrend = (props) => {
    const [twitterTrends, setTwitterTrends] = useState([]);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        // if (ServerConstant.twiteerData.length !== 0) {
        //     setTwitterTrends(ServerConstant.twiteerData);

        // } else {
        getTwitterTrends();
        // }
    }, []);
    const getTwitterTrends = async () => {
        console.log("call api twitter trends");
        showLoader();
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        myHeaders.append('Authorization', 'Bearer ' + '');
        await fetch(ServerConstant.serverPath + "/seller-box/buyboxSellers", {
            method: 'POST',
            headers: myHeaders,
        }).then((response) => response.json())
            .then((result) => {
                console.log(result);
                if (result.status == 1) {
                    setTwitterTrends(result.result.esdata);
                    //  ServerConstant.twiteerData = result.result.trends;
                } else if (result.status == 0) {

                }
                destroyLoader();
            })
            .catch((error) => {
                destroyLoader();
            });
    }

    const destroyLoader = () => {
        setTimeout(() => setLoading(false), 2000);
    }
    const showLoader = () => {
        setLoading(true)
    }
    return (
        <>
            <Loader
                loading={loading} />
            <FlatList
                data={twitterTrends}
                renderItem={({ item, index, }) => (
                    <TouchableOpacity onPress={() => {
                        props.route.navigation.navigation.navigate('SellBoxTrendDetails', { Uri: item._source.productURL })
                    }}>
                        <View style={styles.titleViewStyle}>
                            <View>
                                <Text style={styles.titleTextStyle}>{item._source.title.trim()} </Text>
                            </View>
                            <View>
                                {/* <Text style={styles.timeStyle}>{item.tweet_volume !== null ? CommonFunctions.convertReachedPeople(item.tweet_volume) : 'Count not available'}</Text> */}
                            </View>
                        </View>
                    </TouchableOpacity>
                )}
                keyExtractor={(item, index) => item._id.toString()}
                extraData={twitterTrends}
            />
        </>
    );
}
const styles = StyleSheet.create({
    flex: 1,
    titleViewStyle: {
        marginHorizontal: responsiveWidth(2),
        marginVertical: responsiveWidth(0.8),
        paddingVertical: responsiveWidth(2), paddingHorizontal: responsiveWidth(3),
        backgroundColor: '#ffff',
    },
    titleTextStyle: {
        fontSize: responsiveFontSize(1.6),
        fontFamily: Fonts.Roboto.Roboto_Bold, color: '#444'
    },
    timeStyle: {
        color: '#ababab', flexWrap: 'wrap', fontSize: responsiveFontSize(1.6),
        fontFamily: Fonts.Roboto.Roboto_Regular,
    }

})


export default SellBoxTrend;