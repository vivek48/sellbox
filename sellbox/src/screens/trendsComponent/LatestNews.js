import React, { useState, useEffect } from 'react';
import { Text, Linking, FlatList, TouchableOpacity, View, StyleSheet, BackHandler } from 'react-native';
import { CommonFunctions, Loader, ServerConstant } from '../commoncomponent/index'
import { Images, Fonts, Colors } from '../../styles/Index'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'
const LatestNews = (props) => {
  const [latestNews, setLatestNews] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    getLatestNews();
  }, [])
  const getLatestNews = async () => {
    showLoader();
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
    myHeaders.append('Authorization', 'Bearer ' + '');
    await fetch(ServerConstant.serverPath + "/dataSearch/latestNews", {
      method: 'POST',
      headers: myHeaders,
    }).then((response) => response.json())
      .then((result) => {
        if (result.status == 1) {
          setLatestNews(result.result)

        } else if (result.status == 0) {

        }
        destroyLoader();
      })
      .catch((error) => {
        destroyLoader();
      });
  }

  const destroyLoader = () => {
    setTimeout(() => setLoading(false), 2000);
  }
  const showLoader = () => {
    setLoading(true)
  }
  return (
    <>
      <Loader
        loading={loading} />
      <FlatList
        data={latestNews}
        renderItem={({ item, index, }) => (
          // console.log(item),
          <TouchableOpacity onPress={() => {
            props.route.navigation.navigation.navigate('SellBoxTrendDetails', { Uri: item.link })
          }}>
            <View style={styles.titleViewStyle}>
              <View>
                <Text style={styles.titleTextStyle}>{item.title} </Text>
              </View>
              <View>
                {/* <Text style={styles.timeStyle}>{CommonFunctions.ConvertDate(item.isoDate)} </Text> */}
                {/* {CommonFunctions.ConvertDate(item.isoDate)} */}
              </View>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={(item, index) => item.guid.toString()}
        extraData={latestNews}
      />
    </>
  );
}
const styles = StyleSheet.create({
  flex: 1,
  titleViewStyle: {
    margin: responsiveWidth(2),
    paddingVertical: responsiveWidth(2), paddingHorizontal: responsiveWidth(3),
    backgroundColor: '#ffff',
  },
  titleTextStyle: {
    fontSize: responsiveFontSize(1.6),
    fontFamily: Fonts.Roboto.Roboto_Bold, color: '#444'
  },
  timeStyle: {
    color: '#ababab', flexWrap: 'wrap', fontSize: responsiveFontSize(1.6),
    fontFamily: Fonts.Roboto.Roboto_Regular,
  }

})

export default LatestNews;